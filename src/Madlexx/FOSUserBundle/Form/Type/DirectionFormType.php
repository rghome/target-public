<?php


namespace Madlexx\FOSUserBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\FOSUserBundle\Entity\Repository\UserRepository;
use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class DirectionFormType
 * @package Madlexx\FOSUserBundle\Form\Type
 */
class DirectionFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $directionId = $options['directionId'];

        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'madlexx_fos_user.tables.direction.name'
                ]
            ])
            ->add('admin', EntityType::class, [
                'label'         => false,
                'required' => false,
                'class'         => User::class,
                'query_builder' => function (UserRepository $userRepository) use ($directionId) {
                    return $userRepository->findAllFreeQueryDirectionAdmin($directionId);
                },
                'choice_label'  => 'username',
                'choice_value' => 'id',
                'multiple'      => false,
                'by_reference'  => false,
                'attr'          => [
                    'placeholder' => false,
                    'class' => 'directions-admin-select'
                ],
            ])
            ->add('users', EntityType::class, [
                'label'         => false,
                'required' => false,
                'class'         => User::class,
                'query_builder' => function (UserRepository $userRepository) use ($directionId) {
                    return $userRepository->findAllFreeQueryDirection($directionId);
                },
                'choice_label'  => 'username',
                'multiple'      => true,
                'by_reference'  => false,
                'attr'          => [
                    'placeholder' => false,
                    'class' => 'directions-select'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => !$directionId ?
                    'madlexx_fos_user.direction.actions.create':
                    'madlexx_fos_user.direction.actions.update',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('directionId', null);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'madlexx_user_direction';
    }
}