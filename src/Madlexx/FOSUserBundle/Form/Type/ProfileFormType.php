<?php

namespace Madlexx\FOSUserBundle\Form\Type;

use FOS\UserBundle\Form\Type\ProfileFormType as BaseFormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

use Madlexx\FOSUserBundle\Form\Handler\AvatarHandler;
use Madlexx\FOSUserBundle\Form\Listener\ProfileUploadSubscriber;

class ProfileFormType extends AbstractType
{
    /**
     * @var AvatarHandler
     */
    private $handler;

    /**
     * @param AvatarHandler $avatarHandler
     */
    public function __construct(AvatarHandler $avatarHandler)
    {
        $this->handler = $avatarHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'username',
            null,
            [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'madlexx_fos_user.username',
                ],
            ]
        )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'madlexx_fos_user.username',
                    ],
                ]
            )
            ->add('avatar', FileType::class, [
                'label' => false,
                'required' => false,
                'data_class' => null
            ])
            ->add(
                'removeAvatar',
                CheckboxType::class,
                [
                    'label'  => 'madlexx_fos_user.actions.remove.avatar',
                    'mapped' => false,
                    'required' => false
                ]
            )
            ->add(
                'current_password',
                PasswordType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'form.current_password',
                    ],
                    'translation_domain' => 'FOSUserBundle',
                    'mapped'             => false,
                    'constraints'        => new UserPassword(),
                ]
            )
            ->add('notification', CheckboxType::class, ['label' => 'madlexx_fos_user.actions.notification', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'madlexx_fos_user.actions.update.profile']);


        $builder->addEventSubscriber(new ProfileUploadSubscriber($this->handler));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return BaseFormType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'madlexx_user_profile';
    }
}
