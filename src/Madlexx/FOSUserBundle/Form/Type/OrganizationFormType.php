<?php

namespace Madlexx\FOSUserBundle\Form\Type;

use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\FOSUserBundle\Entity\Repository\DirectionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\FOSUserBundle\Entity\Repository\UserRepository;
use Madlexx\FOSUserBundle\Entity\User;

class OrganizationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $organizationId = $options['organizationId'];
        $isSuperAdmin = $options['isSuperAdmin'];
        $direction = $options['direction'];

        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'madlexx_fos_user.tables.organization.name'
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'madlexx_fos_user.organization.form.phone'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'madlexx_fos_user.organization.form.email',
                ]
            ])
            ->add('site', UrlType::class, [
                'label'    => false,
                'required' => false,
                'attr'     => [
                    'placeholder' => 'madlexx_fos_user.organization.form.site',
                ],
            ])
            ->add('users', EntityType::class, [
                'label'         => false,
                'required' => false,
                'class'         => User::class,
                'query_builder' => function (UserRepository $userRepository) use ($organizationId) {
                    return $userRepository->findAllFreeQuery($organizationId);
                },
                'choice_label'  => 'username',
                'multiple'      => true,
                'by_reference'  => false,
                'attr'          => [
                    'placeholder' => false,
                    'class' => 'organizations-select'
                ],
            ])
            ->add('suspended', CheckboxType::class, [
                'label'    => 'madlexx_fos_user.organization.form.suspended',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                    'label' => !$organizationId ?
                        'madlexx_fos_user.organization.actions.create':
                        'madlexx_fos_user.organization.actions.update',
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();

            if (!$data->getEmail()) {
                return ;
            }

            $form->add('email', EmailType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'madlexx_fos_user.organization.form.email',
                    'readonly' => true
                ]
            ]);
        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use (
            $organizationId, $isSuperAdmin, $direction
        ) {
            $form = $event->getForm();

            if ($isSuperAdmin) {
                $form->add(
                    'direction',
                    EntityType::class,
                    [
                        'label'         => false,
                        'required'      => false,
                        'class'         => Direction::class,
                        'query_builder' => function (DirectionRepository $directionRepository) {
                            return $directionRepository->createQueryBuilder('direction');
                        },
                        'choice_value' => 'id',
                        'choice_label'  => 'name',
                        'multiple'      => false,
                        'by_reference'  => false,
                        'attr'          => [
                            'placeholder' => false,
                            'class'       => 'directions-select'
                        ],
                    ]
                );
            } else {
                $form->add(
                    'direction',
                    EntityType::class,
                    [
                        'label'         => false,
                        'required'      => false,
                        'class'         => Direction::class,
                        'query_builder' => function (DirectionRepository $directionRepository) {
                            return $directionRepository->createQueryBuilder('direction');
                        },
                        'data'          => $direction,
                        'choice_value'  => 'id',
                        'choice_label'  => 'name',
                        'multiple'      => false,
                        'by_reference'  => false,
                        'attr'          => [
                            'placeholder' => false,
                            'class'       => 'directions-select',
                            'disabled'    => true
                        ],
                    ]
                );
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('organizationId', null);
        $resolver->setDefault('isSuperAdmin', null);
        $resolver->setDefault('direction', null);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'madlexx_user_organization';
    }
}
