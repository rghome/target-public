<?php

namespace Madlexx\FOSUserBundle\Form\Type;

use Madlexx\FOSUserBundle\Entity\Repository\OrganizationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class UserFormType
 *
 * @package Madlexx\FOSUserBundle\Form\Type
 */
class UserFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $manager = $options['manager'];
        $roles = $options['roles'];
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => false,
                    'attr'  => [
                        'placeholder' => 'madlexx_fos_user.username',
                    ],
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => false,
                    'disabled' => $options['editAction'],
                    'attr'  => [
                        'placeholder' => 'madlexx_fos_user.email',
                    ],
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'label' => false,
                    'required' => false,
                    'attr'  => [
                        'placeholder' => 'madlexx_fos_user.phone',
                    ],
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => $options['editAction'] ?
                        'madlexx_fos_user.users.actions.update' :
                        'madlexx_fos_user.users.actions.create',
                ]
            )
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($manager, $roles) {
                $form = $event->getForm();

                if (!$manager || in_array(User::ROLE_MANAGER, $manager->getRoles())) {
                    return;
                }

                $form->add(
                    'organization',
                    EntityType::class,
                    [
                        'label'         => false,
                        'required'      => false,
                        'class'         => Organization::class,
                        'query_builder' => function (OrganizationRepository $directionRepository) use ($manager) {
                            $qb = $directionRepository->createQueryBuilder('organization');

                            if (!in_array(User::ROLE_SUPER_ADMIN, $manager->getRoles())) {
                                $qb->where(
                                    $qb->expr()->eq('organization.direction', ':direction')
                                )->setParameter('direction', $manager->getDirection()->getId());
                            }

                            return $qb;
                        },
                        'choice_value' => 'id',
                        'choice_label'  => 'name',
                        'multiple'      => false,
                        'by_reference'  => false,
                        'attr'          => [
                            'placeholder' => false,
                            'class'       => 'organization-select'
                        ],
                    ]
                )
                ->add('roles',
                    ChoiceType::class,
                        [
                        'choices'   => $this->refactorRoles($roles),
                        'multiple'     => true,
                        'attr'      => [
                            'placeholder' => false,
                            'class'       => 'roles-select',
                        ],
                    ])
                ;
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
                'manager'    => null,
                'roles'    => [],
                'editAction' => false,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'madlexx_user';
    }

    /**
     * @param array $roles
     *
     * @return array
     */
    private function refactorRoles(array $roles)
    {
        $roles[User::ROLE_SECONDARY_USER] = 'Can write';
        $roles[User::ROLE_DEFAULT] = 'Read only';

        $cache = [];
        foreach ($roles as $key => $value) {
            $name = (is_array($value) ? $key : $value);
            $cache[ucwords(str_replace('_', ' ', strtolower($name)))] = $key;
        }

        return $cache;
    }
}
