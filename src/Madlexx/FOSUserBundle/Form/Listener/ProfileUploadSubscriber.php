<?php

namespace Madlexx\FOSUserBundle\Form\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Madlexx\FOSUserBundle\Form\Handler\AvatarHandler;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @package Madlexx\FOSUserBundle\Form\Listener
 */
class ProfileUploadSubscriber implements EventSubscriberInterface
{
    /**
     * @var AvatarHandler
     */
    private $handler;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @param AvatarHandler $avatarHandler
     */
    public function __construct(AvatarHandler $avatarHandler)
    {
        $this->handler = $avatarHandler;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::POST_SUBMIT => 'postSubmit',
            FormEvents::PRE_SET_DATA => 'preSetData'
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();

        $this->avatar = $data->getAvatar();
    }
    /**
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        if (!$data) {
            return;
        }

        if (!$data->getAvatar() && $this->avatar) {
            $data->setAvatar($this->avatar);
        }

        if ($form->get('removeAvatar')->getData()) {
            $this->handler->remove($data->getAvatar());
            $data->setAvatar('');
        }

        if ($data->getAvatar() instanceof UploadedFile) {
            $data->setAvatar($this->handler->upload($data->getAvatar()));
        }
    }
}
