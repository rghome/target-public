<?php

namespace Madlexx\FOSUserBundle\Form\Handler;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @package Madlexx\FOSUserBundle\Form\Handler
 */
class AvatarHandler
{
    /**
     * @var string
     */
    private $targetDir;

    private $webDir;

    /**
     * @param string $targetDir
     * @param string $webDir
     */
    public function __construct($targetDir, $webDir)
    {
        $this->targetDir = $targetDir;
        $this->webDir = $webDir;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessClientExtension();
        $file->move($this->targetDir . $this->webDir, $fileName);

        return $this->webDir . '/' . $fileName;
    }

    /**
     * @param $fileName
     */
    public function remove($fileName)
    {
        $file = $this->targetDir . '/' . $fileName;
        if (file_exists($file)) {
            unlink($file);
        }
    }
}
