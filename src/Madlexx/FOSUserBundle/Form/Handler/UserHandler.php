<?php

namespace Madlexx\FOSUserBundle\Form\Handler;

use FOS\UserBundle\Mailer\Mailer;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Form\Form;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class UserHandler
 *
 * @package Madlexx\FOSUserBundle\Form\Handler
 */
class UserHandler
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var MailerInterface
     */
    private $mailer;

    private $tokerGenerator;

    /**
     * UserHandler constructor.
     *
     * @param EntityManager      $entityManager
     * @param Translator         $translator
     * @param ValidatorInterface $validator
     * @param MailerInterface    $mailer
     * @param TokenGenerator     $tokenGenerator
     */
    public function __construct(
        EntityManager $entityManager,
        Translator $translator,
        ValidatorInterface $validator,
        MailerInterface $mailer,
        TokenGenerator $tokenGenerator
    ) {
        $this->em = $entityManager;
        $this->translator = $translator;
        $this->validator = $validator;
        $this->mailer = $mailer;
        $this->tokerGenerator = $tokenGenerator;
    }

    /**
     * @param Form   $form
     * @param User   $user
     * @param User   $manager
     * @param string $message
     * @param string $status
     */
    public function create(Form $form, User &$user, User $manager, &$message, &$status)
    {
        if (!$form->isSubmitted() && !$form->isValid()) {
            $message = $this->getErrorsString($form);
        }

        if (!$user->getOrganization()) {
            $user->setOrganization($manager->getOrganization());
            $user->setDirection($manager->getDirection());
        } else {
            $user->setDirection($user->getOrganization()->getDirection());
        }

        if (!$user->getRoles()) {
            $user->setRoles([User::ROLE_DEFAULT]);
        }
        $user->setEnabled(true);
        $password = md5($user->getUsername().date('now'));
        $user->setPlainPassword('admin'); //$password
        $user->setConfirmationToken($this->tokerGenerator->generateToken());
        $this->em->persist($user);
        try {
            $this->em->flush();
            $this->mailer->sendResettingEmailMessage($user);
            $status = 201;
            $message = $this->translator->trans(
                'madlexx_fos_user.users.form.saved',
                [
                    '%user%' => $form->getData()
                        ->getUsername(),
                ]
            );
        } catch (UniqueConstraintViolationException $e) {
            $message = $e->getMessage();
        }
    }

    /**
     * @param Form   $form
     * @param User   $user
     * @param User   $manager
     * @param string $message
     * @param string $status
     */
    public function update(Form $form, User &$user, User $manager, &$message, &$status)
    {
        if (!$form->isSubmitted() && !$form->isValid()) {
            $message = $this->getErrorsString($form);
        }

        if (!$user->getOrganization()) {
            $user->setOrganization($manager->getOrganization());
        } else {
            $user->setDirection($user->getOrganization()->getDirection());
        }

        if (!$user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokerGenerator->generateToken());
        }

        $this->em->persist($user);
        try {
            $this->em->flush();
            $this->mailer->sendResettingEmailMessage($user);
            $status = 201;
            $message = $this->translator->trans(
                'madlexx_fos_user.users.form.updated',
                [
                    '%user%' => $form->getData()
                        ->getUsername(),
                ]
            );
        } catch (UniqueConstraintViolationException $e) {
            $message = $e->getMessage();
        }
    }
    /**
     * @param Form $form
     *
     * @return string
     */
    private function getErrorsString($form)
    {
        $errors = $this->validator->validate($form);
        $output = '';
        foreach ($errors as $key => $error) {
            $output .= $error->getMessage();
        }

        return $output;
    }
}
