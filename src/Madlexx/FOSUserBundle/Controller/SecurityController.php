<?php

namespace Madlexx\FOSUserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    protected function renderLogin(array $data)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('madlexx_target.main');
        }

        return $this->render('FOSUserBundle:Security:login.html.twig', $data);
    }
}
