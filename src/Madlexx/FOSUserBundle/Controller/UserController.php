<?php

namespace Madlexx\FOSUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\FOSUserBundle\Form\Type\UserFormType;

/**
 * Class UsersController
 *
 * @package Madlexx\FOSUserBundle\Controller
 */
class UserController extends Controller
{

    /**
     * @Route("/users", name="madlexx_target.users")
     * @Template
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', User::ROLE_MANAGER]);

        if (!$this->getUser()->getOrganization() && !$this->isGranted('ROLE_ADMIN')) {
            throw new InvalidArgumentException(
                $this->get('translator')->trans('madlexx_fos_user.errors.without.organization')
            );
        };

        $users = $this->get('serializer')
            ->serialize(
                $this->get('doctrine.orm.default_entity_manager')
                    ->getRepository(User::class)
                    ->getAllUsers($this->getUser()),
                'json'
            );

        return compact('users');
    }

    /**
     * @Route("/users/form", name="madlexx_target.users.form", condition="request.isXmlHttpRequest()")
     * @Template
     *
     * @return array
     */
    public function renderFormAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', User::ROLE_MANAGER]);

        $form = $this->createForm(UserFormType::class, new User(),
            [
                'manager' => $this->getUser(),
                'roles' => $this->getParameter('security.role_hierarchy.roles')
            ]
        )->createView();

        return compact('form');
    }

    /**
     * @Route(
     *     "/users/form/edit/{id}",
     *      name="madlexx_target.users.edit.form_render",
     *     condition="request.isXmlHttpRequest()"
     * )
     * @Template
     * @ParamConverter("user", class="MadlexxFOSUserBundle:User")
     *
     * @param User $user
     *
     * @return array
     */
    public function renderEditFormAction(User $user)
    {
        $form = $this->createForm(
            UserFormType::class,
            $user,
            [
                'manager'  => $this->getUser(),
                'roles' => $this->getParameter('security.role_hierarchy.roles'),
                'editAction' => true,
            ]
        )->createView();

        $id = $user->getId();

        return compact('form', 'id');
    }

    /**
     * @Route("/users/form/create", name="madlexx_target.users.form_create")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $manager = $this->getUser();
        $form = $this->createForm(UserFormType::class, $user, [
            'manager' => $manager,
            'roles' => $this->getParameter('security.role_hierarchy.roles'),
        ]);
        $form->handleRequest($request);

        $status = 400;
        $message = '';

        $this->get('madlexx_fosuserbundle.handler.user_handler')
            ->create($form, $user, $manager, $message, $status);

        return new JsonResponse(
            [
                'message' => $message,
                'user' => json_encode($user)
            ],
            $status
        );
    }

    /**
     * @Route("/users/update/{id}", name="madlexx_target.users.update")
     * @Template
     * @ParamConverter("user", class="MadlexxFOSUserBundle:User")
     *
     * @param User $user
     *
     * @param Request      $request
     *
     * @return JsonResponse
     */
    public function editAction(User $user, Request $request)
    {
        $manager = $this->getUser();
        $form = $this->createForm(
            UserFormType::class,
            $user,
            [
                'manager'  => $manager,
                'roles' => $this->getParameter('security.role_hierarchy.roles'),
                'editAction' => true
            ]
        );
        $form->handleRequest($request);

        $status = 400;
        $message = '';

        $this->get('madlexx_fosuserbundle.handler.user_handler')
            ->update($form, $user, $manager, $message, $status);

        return new JsonResponse(
            [
                'message' => $message,
                'user' => json_encode($user)
            ],
            $status
        );
    }

    /**
     * @Route("/users/delete/{id}", name="madlexx_target.users.delete", condition="request.isXmlHttpRequest()")
     * @ParamConverter("user", class="MadlexxFOSUserBundle:User")
     * @param User $user
     *
     * @return JsonResponse
     */
    public function deleteAction(User $user)
    {
        try {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($user);
            $em->flush();

            $translator = $this->get('translator');

            $message = $translator->trans(
                'madlexx_fos_user.users.messages.removed',
                ['%user%' => $user->getUsername()]
            );
            $status = 200;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status = 500;
        }

        return new JsonResponse(
            [
                'message' => $message
            ],
            $status
        );
    }
}
