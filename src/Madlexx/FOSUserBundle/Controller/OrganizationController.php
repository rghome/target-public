<?php

namespace Madlexx\FOSUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;


use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\FOSUserBundle\Form\Type\OrganizationFormType;
use Madlexx\FOSUserBundle\Controller\Traits\SerializerTrait;

/**
 * @package Madlexx\FOSUserBundle\Controller
 */
class OrganizationController extends Controller
{
    use SerializerTrait;

    /**
     * @Route("/organizations", name="madlexx_target.organization")
     * @Template
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_MANAGER']);
        $serializer = $this->getSerializer();

        $organizations = $serializer
            ->serialize(
                $this->get('doctrine.orm.default_entity_manager')
                    ->getRepository(Organization::class)
                    ->getByDirection(
                        ($this->isGranted('ROLE_SUPER_ADMIN')) ? null : $this->getUser()->getDirection()->getId()
                    ),
                'json'
            );

        return compact('organizations');
    }

    /**
     * @Route("/organization/form", name="madlexx_target.organization.form", condition="request.isXmlHttpRequest()")
     * @Template
     *
     * @return array
     */
    public function renderFormAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_MANAGER']);

        $form = $this->createForm(
            OrganizationFormType::class,
            new Organization(),
            [
                'isSuperAdmin' => $this->isGranted('ROLE_SUPER_ADMIN'),
                'direction' => $this->getUser()->getDirection()
            ]
        )->createView();

        return compact('form');
    }

    /**
     * @Route(
     *     "/organization/form/edit/{id}",
     *      name="madlexx_target.organization.edit.form_render",
     *      condition="request.isXmlHttpRequest()"
     * )
     * @Template
     * @ParamConverter("organization", class="MadlexxFOSUserBundle:Organization")
     *
     * @param Organization $organization
     *
     * @return array
     */
    public function renderEditFormAction(Organization $organization)
    {
        $form = $this->createForm(
            OrganizationFormType::class,
            $organization,
            [
                'organizationId'=>$organization->getId(),
                'isSuperAdmin' => $this->isGranted('ROLE_SUPER_ADMIN'),
                'direction' => $this->getUser()->getDirection()
            ]
        )->createView();

        $id = $organization->getId();

        return compact('form', 'id');
    }

    /**
     * @Route("/organization/form/create", name="madlexx_target.organization.form_create")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $translator = $this->get('translator');
        $organization = new Organization();
        $form = $this->createForm(
            OrganizationFormType::class,
            $organization,
            [
                'isSuperAdmin' => $this->isGranted('ROLE_SUPER_ADMIN'),
                'direction' => $this->getUser()->getDirection()
            ]
        );
        $form->handleRequest($request);

        $status = 400;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            if ($direction = $this->getUser()->getDirection()) {
                $organization->setDirection($direction);
            }

            $em->persist($organization);
            try {
                $em->flush();
                $status = 201;
                $message = $translator->trans(
                    'madlexx_fos_user.organization.form.saved',
                    [
                        '%name%' => $form->getData()
                            ->getName(),
                    ]
                );
            } catch (UniqueConstraintViolationException $e) {
                $message = $e->getMessage();
            }
        } else {
            $message = $this->getErrorsString($form);
        }

        return new JsonResponse(
            [
                'message' => $message,
                'organization' => json_encode($organization)
            ],
            $status
        );
    }

    /**
     * @Route("/organization/update/{id}", name="madlexx_target.organization.update")
     * @Template
     * @ParamConverter("organization", class="MadlexxFOSUserBundle:Organization")
     *
     * @param Organization $organization
     *
     * @param Request      $request
     *
     * @return JsonResponse
     */
    public function editAction(Organization $organization, Request $request)
    {
        $translator = $this->get('translator');
        $form = $this->createForm(
            OrganizationFormType::class,
            $organization,
            [
                'organizationId'=>$organization->getId(),
                'isSuperAdmin' => $this->isGranted('ROLE_SUPER_ADMIN'),
                'direction' => $this->getUser()->getDirection()
            ]
        );
        $form->handleRequest($request);

        $status = 400;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            try {
                $em->persist($organization);
                $em->flush();
                $status = 201;
                $message = $translator->trans(
                    'madlexx_fos_user.organization.form.saved',
                    [
                        '%name%' => $form->getData()
                            ->getName(),
                    ]
                );
            } catch (UniqueConstraintViolationException $e) {
                $message = $e->getMessage();
            }
        } else {
            $message = $this->getErrorsString($form);
        }

        return new JsonResponse(
            [
                'message' => $message,
                'organization' => json_encode($organization)
            ],
            $status
        );
    }

    /**
     * @Route("/organization/delete/{id}", name="madlexx_target.organization.delete")
     * @ParamConverter("organization", class="MadlexxFOSUserBundle:Organization")
     * @param Organization $organization
     *
     * @return JsonResponse
     */
    public function deleteAction(Organization $organization)
    {
        try {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($organization);
            $em->flush();

            $translator = $this->get('translator');

            $message = $translator->trans(
                'madlexx_fos_user.organization.messages.removed',
                ['%organization%' => $organization->getName()]
            );
            $status = 200;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status = 500;
        }

        return new JsonResponse(
            [
                'message' => $message
            ],
            $status
        );
    }

    /**
     * @param Form $form
     *
     * @return string
     */
    private function getErrorsString($form)
    {
        $errors = $this->get('validator')->validate($form);
        $output = '';
        foreach ($errors as $key => $error) {
            $output .= $error->getMessage();
        }

        return $output;
    }
}
