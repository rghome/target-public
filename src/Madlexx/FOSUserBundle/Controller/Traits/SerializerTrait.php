<?php

namespace Madlexx\FOSUserBundle\Controller\Traits;

use Madlexx\FOSUserBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait SerializerTrait
{
    /**
     * @return Serializer
     */
    private function getSerializer()
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();

        $normalizer->setCircularReferenceHandler(function ($object) {
            if (!$object instanceof User) {
                return $object->getName();
            }
        });

        $serializer = new Serializer(array($normalizer), array($encoder));

        return $serializer;
    }
}
