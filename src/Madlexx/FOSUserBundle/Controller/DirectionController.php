<?php


namespace Madlexx\FOSUserBundle\Controller;


use Madlexx\FOSUserBundle\Controller\Traits\SerializerTrait;
use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\FOSUserBundle\Form\Type\DirectionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class DirectionController
 * @package Madlexx\FOSUserBundle\Controller
 */
class DirectionController extends Controller
{
    use SerializerTrait;

    /**
     * @Route("/directions", name="madlexx_target.direction")
     * @Template
     *
     * @return array
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_SUPER_ADMIN']);
        $serializer = $this->getSerializer();

        $directions = $serializer
            ->serialize(
                $this->get('doctrine.orm.default_entity_manager')
                     ->getRepository(Direction::class)
                     ->findAll(),
                'json'
            );

        return compact('directions');
    }

    /**
     * @Route("/direction/form", name="madlexx_target.direction.form", condition="request.isXmlHttpRequest()")
     * @Template
     *
     * @return array
     */
    public function renderFormAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_SUPER_ADMIN']);

        $form = $this->createForm(DirectionFormType::class, new Direction())->createView();

        return compact('form');
    }

    /**
     * @Route(
     *     "/direction/form/edit/{id}",
     *      name="madlexx_target.direction.edit.form_render",
     *      condition="request.isXmlHttpRequest()"
     * )
     * @Template
     * @ParamConverter("direction", class="MadlexxFOSUserBundle:Direction")
     *
     * @param Direction $direction
     *
     * @return array
     */
    public function renderEditFormAction(Direction $direction)
    {
        $form = $this->createForm(
            DirectionFormType::class,
            $direction,
            ['directionId'=>$direction->getId()]
        )->createView();

        $id = $direction->getId();

        return compact('form', 'id');
    }

    /**
     * @Route("/direction/form/create", name="madlexx_target.direction.form_create")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $translator = $this->get('translator');
        $direction = new Direction();
        $form = $this->createForm(DirectionFormType::class, $direction);
        $form->handleRequest($request);

        $status = 400;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($direction);
            try {
                $em->flush();
                $status = 201;
                $message = $translator->trans(
                    'madlexx_fos_user.direction.form.saved',
                    [
                        '%name%' => $form->getData()
                                         ->getName(),
                    ]
                );
            } catch (UniqueConstraintViolationException $e) {
                $message = $e->getMessage();
            }
        } else {
            $message = $this->getErrorsString($form);
        }

        return new JsonResponse(
            [
                'message' => $message,
                'direction' => json_encode($direction)
            ],
            $status
        );
    }

    /**
     * @Route("/direction/update/{id}", name="madlexx_target.direction.update")
     * @Template
     * @ParamConverter("direction", class="MadlexxFOSUserBundle:Direction")
     *
     * @param Direction $direction
     *
     * @param Request      $request
     *
     * @return JsonResponse
     */
    public function editAction(Direction $direction, Request $request)
    {
        $translator = $this->get('translator');
        $form = $this->createForm(
            DirectionFormType::class,
            $direction,
            ['directionId'=>$direction->getId()]
        );
        $form->handleRequest($request);

        $status = 400;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            try {
                $em->persist($direction);
                $em->flush();
                $status = 201;
                $message = $translator->trans(
                    'madlexx_fos_user.direction.form.saved',
                    [
                        '%name%' => $form->getData()
                                         ->getName(),
                    ]
                );
            } catch (UniqueConstraintViolationException $e) {
                $message = $e->getMessage();
            }
        } else {
            $message = $this->getErrorsString($form);
        }

        return new JsonResponse(
            [
                'message' => $message,
                'direction' => json_encode($direction)
            ],
            $status
        );
    }

    /**
     * @Route("/direction/delete/{id}", name="madlexx_target.direction.delete")
     * @ParamConverter("direction", class="MadlexxFOSUserBundle:Direction")
     * @param Direction $direction
     *
     * @return JsonResponse
     */
    public function deleteAction(Direction $direction)
    {
        try {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($direction);
            $em->flush();

            $translator = $this->get('translator');

            $message = $translator->trans(
                'madlexx_fos_user.direction.messages.removed',
                ['%direction%' => $direction->getName()]
            );
            $status = 200;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status = 500;
        }

        return new JsonResponse(
            [
                'message' => $message
            ],
            $status
        );
    }

    /**
     * @param Form $form
     *
     * @return string
     */
    private function getErrorsString($form)
    {
        $errors = $this->get('validator')->validate($form);
        $output = '';
        foreach ($errors as $key => $error) {
            $output .= $error->getMessage();
        }

        return $output;
    }
}