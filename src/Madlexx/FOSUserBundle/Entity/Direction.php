<?php


namespace Madlexx\FOSUserBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Madlexx\TargetBundle\Entity\Chat;
use Madlexx\TargetBundle\Entity\Document;
use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;
use Madlexx\TargetBundle\Entity\Event;
use Madlexx\TargetBundle\Entity\Message;
use Madlexx\TargetBundle\Entity\Query;
use Madlexx\TargetBundle\Entity\Report;
use Madlexx\TargetBundle\Entity\Response;

/**
 * Class Direction
 * @package Madlexx\FOSUserBundle\Entity
 */
class Direction implements \JsonSerializable
{
    use DateTimeTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Collection
     */
    private $users;

    /**
     * @var User
     */
    private $admin;

    /**
     * @var Collection
     */
    private $organizations;

    /**
     * @var Collection
     */
    private $reports;

    /**
     * @var Collection
     */
    private $events;

    /**
     * @var Collection
     */
    private $documents;

    /**
     * @var Collection
     */
    private $querys;

    /**
     * @var Collection
     */
    private $messages;

    /**
     * @var Collection
     */
    private $chats;

    /**
     * @var Collection
     */
    private $responses;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->organizations = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->querys = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->chats = new ArrayCollection();
        $this->responses = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return $this
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->setDirection(null);
        }
    }

    /**
     * Get users
     *
     * @return Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param User $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * Add Organization
     *
     * @param Organization $organization
     *
     * @return $this
     */
    public function addOrganization(Organization $organization)
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove organization
     *
     * @param Organization $organization
     */
    public function removeOrganization(Organization $organization)
    {
        if ($this->organizations->contains($organization)) {
            $this->organizations->removeElement($organization);
            $organization->setDirection(null);
        }
    }

    /**
     * Get organizations
     *
     * @return Collection
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }

    /**
     * Add Report
     *
     * @param Report $report
     *
     * @return $this
     */
    public function addReport(Report $report)
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Report
     *
     * @param Report $report
     */
    public function removeReport(Report $report)
    {
        if ($this->reports->contains($report)) {
            $this->reports->removeElement($report);
            $report->setDirection(null);
        }
    }

    /**
     * Get reports
     *
     * @return Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Add Event
     *
     * @param Event $event
     *
     * @return $this
     */
    public function addEvent(Event $event)
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Event
     *
     * @param Event $event
     */
    public function removeEvent(Event $event)
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->setDirection(null);
        }
    }

    /**
     * Get events
     *
     * @return Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add Document
     *
     * @param Document $document
     *
     * @return $this
     */
    public function addDocument(Document $document)
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Document
     *
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            $document->setDirection(null);
        }
    }

    /**
     * Get documents
     *
     * @return Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Add Query
     *
     * @param Query $query
     *
     * @return $this
     */
    public function addQuery(Query $query)
    {
        if (!$this->querys->contains($query)) {
            $this->querys[] = $query;
            $query->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Query
     *
     * @param Query $query
     */
    public function removeQuery(Query $query)
    {
        if ($this->querys->contains($query)) {
            $this->querys->removeElement($query);
            $query->setDirection(null);
        }
    }

    /**
     * Get querys
     *
     * @return Collection
     */
    public function getQuerys()
    {
        return $this->querys;
    }

    /**
     * Add Message
     *
     * @param Message $message
     *
     * @return $this
     */
    public function addMessage(Message $message)
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Message
     *
     * @param Message $messages
     */
    public function removeMessage(Message $messages)
    {
        if ($this->messages->contains($messages)) {
            $this->messages->removeElement($messages);
            $messages->setDirection(null);
        }
    }

    /**
     * Get messages
     *
     * @return Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add Chat
     *
     * @param Chat $chat
     *
     * @return $this
     */
    public function addChat(Chat $chat)
    {
        if (!$this->chats->contains($chat)) {
            $this->chats[] = $chat;
            $chat->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Chat
     *
     * @param Chat $chat
     */
    public function removeChat(Chat $chat)
    {
        if ($this->chats->contains($chat)) {
            $this->chats->removeElement($chat);
            $chat->setDirection(null);
        }
    }

    /**
     * Get chats
     *
     * @return Collection
     */
    public function getChat()
    {
        return $this->chats;
    }

    /**
     * Add Response
     *
     * @param Response $response
     *
     * @return $this
     */
    public function addResponse(Response $response)
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setDirection($this);
        }

        return $this;
    }

    /**
     * Remove Response
     *
     * @param Response $response
     */
    public function removeResponse(Response $response)
    {
        if ($this->responses->contains($response)) {
            $this->responses->removeElement($response);
            $response->setDirection(null);
        }
    }

    /**
     * Get responses
     *
     * @return Collection
     */
    public function getResponse()
    {
        return $this->responses;
    }

    /**
     * @return int
     */
    public function getUsersCount()
    {
        return count($this->users);
    }

    /**
     * @return int
     */
    public function getOrganizationsCount()
    {
        return count($this->organizations);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id'                    => $this->getId(),
            'name'                  => $this->getName(),
            'admin'                 => $this->getAdmin()->getUsername(),
            'usersCount'            => $this->getUsersCount(),
            'organizationsCount'    => $this->getOrganizationsCount()
        ];
    }
}