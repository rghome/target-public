<?php

namespace Madlexx\FOSUserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 */
class User extends BaseUser implements \JsonSerializable, EquatableInterface
{
    use DateTimeTrait;

    /**
     *
     */
    const ROLE_MANAGER = 'ROLE_MANAGER';
    /**
     *
     */
    const ROLE_ADMIN = 'ROLE_ADMIN';
    /**
     *
     */
    const ROLE_SECONDARY_USER = 'ROLE_SECONDARY_USER';
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var Organization
     */
    private $organization;

    /**
     * @var Direction
     */
    private $direction;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var bool
     */
    protected $manager;

    /**
     * @var bool
     */
    protected $notification;

    /**
     * @var string
     */
    private $custom_role;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return $this
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     *
     * @return $this
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Get direction
     *
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set direction
     *
     * @param Direction $direction
     *
     * @return $this
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSecondaryUser()
    {
        return $this->hasRole(self::ROLE_SECONDARY_USER);
    }

    /**
     * @param bool $state
     *
     * @return $this
     */
    public function setSecondaryUser($state = false)
    {
        $role = $state ? self::ROLE_SECONDARY_USER : self::ROLE_DEFAULT;
        $this->setRoles([$role]);

        return $this;
    }

    /**
     * @return bool
     */
    public function isManager()
    {
        return $this->hasRole(self::ROLE_MANAGER);
    }

    /**
     * @param bool $state
     *
     * @return $this
     */
    public function setManager($state = false)
    {
        $role = $state ? self::ROLE_MANAGER : self::ROLE_DEFAULT;
        $this->setRoles([$role]);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * @return bool
     */
    public function isNotification()
    {
        return $this->notification;
    }

    /**
     * @param $notification
     *
     * @return $this
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id'           => $this->getId(),
            'username'     => $this->getUsername(),
            'email'        => $this->getEmail(),
            'phone'        => $this->getPhone(),
            'organization' => $this->getOrganization() ? $this->getOrganization()
                ->getName() : '',
            'direction' => $this->getDirection() ? $this->getDirection()
                                                              ->getName() : '',

            'joined'       => $this->getCreatedAt()->format('m/d/Y'),
        ];
    }

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user)
    {
        if ($user instanceof User) {
            // Step One
            if (
                ((count($this->getRoles()) == 1) && (count($user->getRoles()) == 1)) &&
                (($this->getRoles()[0] == User::ROLE_DEFAULT) && ($this->getRoles()[0] == User::ROLE_DEFAULT))
            ) {
                return false;
            }

            // Check that the roles are the same, in any order
            $isEqual = count($this->getRoles()) == count($user->getRoles());
            if ($isEqual) {
                foreach($this->getRoles() as $role) {
                    $isEqual = $isEqual && in_array($role, $user->getRoles());
                }
            }
            return $isEqual;
        }

        return false;
    }
}
