<?php

namespace Madlexx\FOSUserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class OrganizationRepository
 *
 * @package Madlexx\FOSUserBundle\Entity\Repository
 */
class OrganizationRepository extends EntityRepository
{
    /**
     * @param null $directionId
     *
     * @return array
     */
    public function getByDirection($directionId = null)
    {
        if (is_null($directionId)) {
            return $this->findAll();
        }

        return $this->findBy([
            'direction' => $directionId
        ]);
    }
}
