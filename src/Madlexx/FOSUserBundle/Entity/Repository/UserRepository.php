<?php

namespace Madlexx\FOSUserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

use Madlexx\FOSUserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    /**
     * Find all users without organization.
     *
     * @param null $organizationId
     *
     * @return QueryBuilder
     */
    public function findAllFreeQuery($organizationId = null)
    {
        $qb = $this->createQueryBuilder('user');

        $qb->select('user')
            ->where(
                $qb->expr()->notLike('user.roles', ':role_admin')
            )->setParameter('role_admin', '%"'.User::ROLE_SUPER_ADMIN.'"%');

        if (!is_null($organizationId)) {
            $qb->leftJoin('user.organization', 'organization');
            $qb->andWhere(
                $qb->expr()
                    ->orX(
                        $qb->expr()
                            ->eq('organization.id', ':id'),
                        $qb->expr()
                            ->isNull('user.organization')
                    )
            )->setParameter('id', $organizationId);
        } else {
            $qb->andWhere(
                $qb->expr()->isNull('user.organization')
            );
        }

        return $qb;
    }

    /**
     * Find all users without direction.
     *
     * @param null $directionId
     *
     * @return QueryBuilder
     */
    public function findAllFreeQueryDirectionAdmin($directionId = null)
    {
        $qb = $this->createQueryBuilder('user');

        $qb->select('user')
            ->where(
                $qb->expr()->like('user.roles', ':role_admin')
            )->setParameter('role_admin', '%"'.User::ROLE_ADMIN.'"%');


        $qb->andWhere(
            $qb->expr()->isNull('user.direction')
        );

        if ($directionId !== null) {
            $qb->orWhere(
                $qb->expr()->eq('user.direction', ':directionId')
            )->setParameter('directionId', $directionId);
        }

        return $qb;
    }

    /**
     * Find all users without direction.
     *
     * @param null $directionId
     *
     * @return QueryBuilder
     */
    public function findAllFreeQueryDirection($directionId = null)
    {
        $qb = $this->createQueryBuilder('user');

        $qb->select('user')
            ->where(
                $qb->expr()->notLike('user.roles', ':role_admin')
            )->setParameter('role_admin', '%"'.User::ROLE_SUPER_ADMIN.'"%');

        if (!is_null($directionId)) {
            $qb->leftJoin('user.direction', 'direction');
            $qb->andWhere(
                $qb->expr()
                   ->orX(
                       $qb->expr()
                          ->eq('direction.id', ':id'),
                       $qb->expr()
                          ->isNull('user.direction')
                   )
            )->setParameter('id', $directionId);
        } else {
            $qb->andWhere(
                $qb->expr()->isNull('user.direction')
            );
        }

        return $qb;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getAllUsers(User $user)
    {
        $qb = $this->createQueryBuilder('user');

        $qb->select('user');

        $parameters = [
            'role_admin'   => User::ROLE_SUPER_ADMIN,
            'current_user' => $user->getId(),
//            'directionId' => $user->getDirection()->getId()
        ];

        if (!in_array(User::ROLE_SUPER_ADMIN, $user->getRoles())) {
            $qb->leftJoin('user.direction', 'direction');
            $qb->andWhere(
                $qb->expr()->eq('direction.id', ':directionId')
            );
            $parameters['directionId'] = $user->getDirection()->getId();
        }

        $qb->andWhere(
            $qb->expr()->andX(
                $qb->expr()->notLike('user.roles', ':role_admin'),
                $qb->expr()->neq('user.id', ':current_user')
            )
        )->setParameters($parameters);

        return $qb->getQuery()->getResult();
    }
}
