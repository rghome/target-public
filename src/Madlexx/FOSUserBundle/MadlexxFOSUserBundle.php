<?php

namespace Madlexx\FOSUserBundle;

use Madlexx\FOSUserBundle\DependencyInjection\MadlexxFOSUserExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @package Madlexx\FOSUserBundle
 */
class MadlexxFOSUserBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new MadlexxFOSUserExtension();
    }
}
