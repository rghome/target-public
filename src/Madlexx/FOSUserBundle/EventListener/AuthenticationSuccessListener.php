<?php


namespace Madlexx\FOSUserBundle\EventListener;

//InteractiveLoginEvent
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthenticationSuccessListener
{
    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        fwrite(fopen('/tmp/dump', 'w'), print_r($event->getAuthenticationToken()->getUser()->getId(), 1));
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        fwrite(fopen('/tmp/dump', 'w'), print_r(21312, 1));
    }
}