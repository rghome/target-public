<?php


namespace Madlexx\FOSUserBundle\EventListener;

use Madlexx\FOSUserBundle\Entity\User;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class RequestListener
 * @package Madlexx\FOSUserBundle\EventListener
 */
class RequestListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Router
     */
    private $router;

    /**
     * RequestListener constructor.
     *
     * @param ContainerInterface $container
     * @param Router $router
     */
    public function __construct(ContainerInterface $container, Router $router)
    {
        $this->container = $container;
        $this->router = $router;
    }

    /**
     * @param GetResponseEvent $event
     *
     * @return bool
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return true;
        }

        /**
         * e.g. anonymous authentication
         *
         * @var User $user
         */
        if (!is_object($user = $token->getUser())) {
            return true;
        }

        /**
         * Only users, without admin
         */
        if (!is_object($user->getOrganization())) {
            return true;
        }

        /**
         * If organization is suspended need to logout user
         */
        if ($user->getOrganization()->isSuspended()) {
            $this->doSuspend($event);
        }
    }

    /**
     * @param GetResponseEvent $event
     */
    private function doSuspend(GetResponseEvent $event)
    {
        $this->container->get('security.token_storage')->setToken(null);
        $event->getRequest()->getSession()->invalidate();

        $this->container->get('session')
            ->getFlashBag()
            ->set('organizationWasSuspended', 'Your organization was suspended')
        ;

        $routeName = $event->getRequest()->get('_route');
        $url = $this->router->generate($routeName);

        $event->setResponse(new RedirectResponse($url));
    }
}