<?php

namespace Madlexx\TargetBundle\Handler;

use Madlexx\FOSUserBundle\Entity\User;

class EmailHandler
{
    private $mailer;

    private $twig;

    /**
     * @param mixed $mailer
     *
     * @return EmailHandler
     */
    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;

        return $this;
    }

    /**
     * @param mixed $twig
     *
     * @return EmailHandler
     */
    public function setTwig(\Twig_Environment $twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * @param User   $user
     * @param        $title
     * @param string $subject
     */
    public function sendNotification(User $user, $title, $subject = '')
    {
        if (!$user->isNotification()) {
            return ;
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('New notification' . $subject)
            ->setFrom('no-reply@target.mail')
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render(
                    'MadlexxTargetBundle:Email:notification.html.twig',
                    [
                        'title' => $title,
                        'user'  => $user
                    ]
                ),
                'text/html'
            );

        $this->mailer->send($message);
    }
}
