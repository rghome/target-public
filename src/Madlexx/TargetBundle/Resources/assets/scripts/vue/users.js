Vue.config.debug = true;
Vue.config.devtools = true;

var vm = new Vue({
    el: '#users-vue',
    components: {
        notification: Notification
    },
    data: {
        users: pageData || [],
        head: {
            name: 'Name',
            organization: 'Organization',
            actions: 'Actions',
            email: 'Email',
            phone: 'Phone',
            joined: 'Joined'
        },
        actions: actions,
        button: button,
        body: '',
        show: false,
        editForm: '',
        showEditForm: false,
        index: ''
    },
    ready: function () {
        this.getForm();
        setTimeout(function () {
            $('#users').DataTable({
                "info": false,
                bFilter: false,
                "bLengthChange": false,
                "order": [[ 3, "asc" ]],
                columnDefs: [
                    { orderable: false, targets: -1 }
                ]
            });
        }, 50);
    },
    methods: {
        getForm: function () {
            this.$http.get(URL.form).then(function (response) {
                this.body = response.data;
                this.resetJsForm();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });

        },
        create: function (e) {
            e.preventDefault();
            var data = $(e.target).serializeObject();

            this.$http.post(URL.create, data, {emulateJSON: true}).then(function(response){
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.users.push(
                    JSON.parse(
                        JSON.parse(response.body).user
                    )
                );
                this.body = '';
                this.getForm();
                this.show = false;
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        getEditForm: function (id) {
            this.$http.get(URL.edit + id).then(function (response) {
                this.editForm = response.data;
                this.showEditForm = true;
                this.resetJsForm();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        update: function (e) {
            e.preventDefault();
            var data = $(e.target).serializeObject();
            var actionUrl = $(e.target).prop('action');
            this.$http.post(actionUrl, data, {emulateJSON: true}).then(function(response){
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.users.$set(this.index,
                    JSON.parse(
                        JSON.parse(response.body).user
                    )
                );
                this.index = '';
                this.editForm = '';
                this.resetJsForm();
                this.showEditForm = false;
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        resetJsForm: function () {
            setTimeout(function () {
                $.material.init();
                $('.organization-select').select2({  dropdownAutoWidth : true});

                var selected = $(".roles-select :selected").map(function(){ return this.value })
                    .get();
                var uniqueSelected = selected.filter(function(item, pos) {
                    return selected.indexOf(item) == pos;
                });

                if (uniqueSelected.length > 1) {
                    $(".roles-select option[value='ROLE_USER']").prop("selected", false);
                }

                $('.roles-select').select2({  dropdownAutoWidth : true});
                $('#madlexx_user').on('submit', function (e) {
                    vm.create(e);
                })
                $('#madlexx_user_update').on('submit', function (e) {
                    vm.update(e);
                })
            },200);
        }
    }
});