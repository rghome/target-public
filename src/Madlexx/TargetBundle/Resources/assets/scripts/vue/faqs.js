Vue.config.debug = true;
Vue.config.devtools = true;

var vm = new Vue({
    el: '#faqs-vue',
    components: {
        notification: Notification
    },
    data: {
        faqs: pageData || [],
        head: {
            question: 'Question',
            position: 'Position',
            enabled: 'Enabled',
            updated: 'Updated at',
            actions: 'Actions'
        },
        actions: actions
    }
});