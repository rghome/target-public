Vue.config.debug = true;
Vue.config.devtools = true;

var DashboardVue = new Vue({
    el: '#dashboard-vue',
    components: {
        notification: Notification,
    },
});
