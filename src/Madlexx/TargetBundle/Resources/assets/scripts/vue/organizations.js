Vue.config.debug = true;
Vue.config.devtools = true;

var vm = new Vue({
    el: '#organizations-vue',
    components: {
        notification: Notification
    },
    data: {
        organizations: pageData || [],
        head: {
            name: 'Name',
            direction: 'Direction',
            userCount: 'Number of the users',
            actions: 'Actions',
            email: 'Email',
            phone: 'Phone',
            suspended: 'Suspended'
        },
        actions: actions,
        button: button,
        body: '',
        show: false,
        editForm: '',
        showEditForm: false,
        index: ''
    },
    ready: function () {
        this.getForm();
        setTimeout(function () {
            $('#organizations').DataTable({
                "info": false,
                bFilter: false,
                "bLengthChange": false,
                columnDefs: [
                    { orderable: false, targets: -1 }
                ]
            });
        }, 50);
    },
    methods: {
        getForm: function () {
            this.$http.get(URL.form).then(function (response) {
                this.body = response.data;
                this.resetJsForm();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        create: function (e) {
            e.preventDefault();
            var data = $(e.target).serializeObject();

            this.$http.post(URL.create, data, {emulateJSON: true}).then(function(response){
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.organizations.push(
                    JSON.parse(
                        JSON.parse(response.body).organization
                    )
                );
                this.body = '';
                this.getForm();
                this.show = false;
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        update: function (e) {
            e.preventDefault();
            var data = $(e.target).serializeObject();
            var actionUrl = $(e.target).prop('action');
            this.$http.post(actionUrl, data, {emulateJSON: true}).then(function(response){
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.organizations.$set(this.index,
                    JSON.parse(
                        JSON.parse(response.body).organization
                    )
                );
                this.index = '';
                this.editForm = '';
                this.resetJsForm();
                this.showEditForm = false;
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        getEditForm: function (id) {
            this.$http.get(URL.edit + id).then(function (response) {
                this.editForm = response.data;
                this.showEditForm = true;
                this.resetJsForm();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        resetJsForm: function () {
            setTimeout(function () {
                $.material.init();
                $('.organizations-select').select2({  dropdownAutoWidth : true});
                $('.directions-select').select2({  dropdownAutoWidth : true});
                $('#madlexx_user_organization').on('submit', function (e) {
                    vm.create(e);
                })
                $('#madlexx_user_organization_update').on('submit', function (e) {
                    vm.update(e);
                })
            },200);
        }
    }
});
