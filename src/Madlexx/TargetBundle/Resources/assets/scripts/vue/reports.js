'use strict';

Vue.config.debug = true;
Vue.config.devtools = true;

var reportsVM = new Vue({
    el: '#reports-vue',
    components: {
        notification: Notification
    },
    data: {
        form: '',
        show: false,
        reports: [],
        sorting: 'DESC',
        busy: false,
        end : false,
        offset: 0,
        label: {
            header: messages.reports.header,
            emptyPlaceholder: messages.reports.emptyPlaceholder,
            added: messages.label.added,
            title: messages.label.title,
            button: messages.label.button,
            drop: messages.label.drop
        }
    },
    ready: function () {
        this.getReports();
        this.scrollLoad();
    },
    methods: {
        getReports: function () {
            var $this = this;
            var getUrl = URL.reports.all+'?sorting=' + this.sorting + '&offset=' + this.offset;
            this.busy = true;
            this.$http.get(getUrl).then(function (response) {
                var reports = response.data.reports;
                if (reports.length == 0 ) {
                    this.end = true;
                    return ;
                }
                if (this.reports.length > 0) {
                    reports.forEach(function (item) {
                        $this.reports.push(item);
                    });
                } else {
                    this.reports = reports;
                }
                this.busy = false;

                this.showPdf();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                this.busy = false;
            });
        },
        getForm: function () {
            this.$http.get(URL.reports.create).then(function (response) {
                this.form = response.data.form;
                setTimeout(function(){
                    var element = $('#report_file_form');
                    this.$compile(element.get(0));
                }.bind(this), 250);

            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        reload: function () {
            this.show = false;
            this.getReports();
        },

        remove: function (id) {
            this.$http.get(URL.reports.remove + id).then(function (response) {
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.getReports();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },

        showPdf: function () {
            $("#pdf-head").text(this.reports[0].name);
            PDFObject.embed('/' + this.reports[0].path , "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },

        changeSorting: function () {
            this.sorting = this.sorting == 'DESC' ? 'ASC' : 'DESC';
            this.offset = 0;
            this.end = 0;
            this.reports = [];
            this.getReports();
        },

        scrollLoad: function () {
            var $this = this;
            $(function () {
                var element = $('#reports-vue .scroll-zone');
                element.scroll(function () {
                    var elHeight = element[0].scrollHeight - element.height();
                    if (elHeight - 10 <= element.scrollTop() && !$this.busy && !$this.end) {
                        $this.offset += 5;
                        $this.getReports();
                    }
                });
            });
        },
        setFavorite: function (file_id, e) {
            favoritesVM.$emit('createFavorite', file_id, e);
        }
    }
});