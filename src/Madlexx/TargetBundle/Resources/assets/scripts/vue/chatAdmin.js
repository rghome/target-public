if (admin){
    var chatAdmin = new Vue({
        el: '#chat-vue-admin',
        components: {
            notification: Notification
        },
        data: {
            messages: [],
            message: {},
            chats: [],
            user: 0,
            chat: 0,
            show: false,
        },
        ready: function () {
            var $this = this;
            $(function () {
                $this.getChats();
            })
        },
        methods: {
            getChats: function () {
                this.$http.get(chatUrl.getAll).then(function (response) {
                    this.chats = response.data.chats;
                    this.user = response.data.user;
                    setTimeout(function () {
                        $('#chat-table').DataTable({
                            "info": false,
                            bFilter: false,
                            "bLengthChange": false,
                            columnDefs: [
                                { orderable: false, targets: -1 }
                            ]
                        });
                    }, 200);
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            getMessages: function (id) {
                this.$http.get(chatUrl.getById + '/' + id).then(function (response) {
                    this.messages = response.data.messages;
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            send: function (message) {
                if (message.length < 3) {
                    Methods.showErrorMessage(messages.error, 'Message cannot be less than 3.');
                    return;
                }
                this.$http.post(chatUrl.create, {
                    'chat': this.chat,
                    'message': message
                }, {emulateJSON: true}).then(function (response) {
                    this.messages.unshift(response.data.message);
                    this.scrollToTop();
                },function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            openChat: function (id, number) {
                this.chat = id;
                this.getMessages(id);
                if (number > 0) {
                    this.clearCount();
                }
            },
            clearCount: function () {
                this.$http.post(chatUrl.clear, {
                    'chat': this.chat,
                }, {emulateJSON: true}).then(function (response) {

                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            scrollToTop: function () {
                var chatElement = $('.chat-message');
                chatElement.animate({ scrollTop: 0 }, "slow");
            },
            upload: function (file) {
                var form = new FormData();
                form.append('file', file);
                form.append('chat', this.chat);

                this.$http.post(chatUrl.upload, form, {emulateJSON: true}).then(
                    function (response) {
                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                        this.getMessages(this.chat);
                    },
                    function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    }
                );
            }
        }
    });

    if (!window.chatAdmin) {
        window.chatAdmin = chatAdmin;
    }
}