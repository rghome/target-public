if (! admin) {
    var pqVM = new Vue({
        el: '#private-query-vue',
        components: {
            notification: Notification,
        },
        data: {
            form: '',
            show: false,
            queries: [],
            user: 0,
            label: {
                header: messages.queries.header,
                viewedByAdmin: messages.queries.viewedByAdmin,
                view: messages.queries.view,
                emptyPlaceholder: messages.queries.emptyPlaceholder,
                forPublication: messages.label.forPublication,
                added: messages.label.added,
                name: messages.label.titleQuery,
                title: messages.queries.title,
                button: messages.queries.create,
                response: {
                    singular: messages.label.response,
                    create: messages.response.create
                }
            }
        },
        ready: function () {
            this.getPrivateQueries();
            this.getForm();
        },
        methods: {
            setFavorite: function (file_id, e) {
                favoritesVM.$emit('createFavorite', file_id, e);
            },
            getPrivateQueries: function () {
                this.$http.get(URL.privateQuery.all).then(function (response) {
                    var responseQueries = response.data.queries;
                    this.user = response.data.user;
                    this.queries = responseQueries;
                    if (responseQueries.length > 4) {
                        this.queries = responseQueries.slice(1).slice(-4);
                    }

                    setTimeout(function () {
                        $('#private-queries-table').DataTable({
                            "info": false,
                            bFilter: false,
                            "bLengthChange": false,
                            "order": [[3, "desc"]],
                            "aoColumnDefs": [
                                {'bSortable': false, 'aTargets': [5]}
                            ]
                        });
                    }, 50);

                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            getForm: function () {
                this.$http.get(URL.privateQuery.create).then(function (response) {
                    this.form = response.data.form;
                    setTimeout(function () {
                        $.material.init();
                        $('form[name=query_form]').on('submit', function (event) {
                            pqVM.submit(event);
                        });
                    }, 200);
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            submit: function (event) {
                event.preventDefault();
                var data = $(event.target).serializeObject();
                this.$http.post(URL.privateQuery.create, data, {emulateJSON: true}).then(function (response) {
                    this.queries.push(response.data.query);
                    if (this.queries.length > 4) {
                        this.queries.splice(0,1);
                    }
                    Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                    this.show = false;
                    $('form[name=query_form]').get(0).reset();
                }), function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                };
            },
            send: function (id, message, index) {
                this.$http.post(URL.adminQuery.send,
                    {
                        'id': id,
                        'message': message
                    }, {emulateJSON: true}).then(function (response) {
                    this.queries[index].responses.push(response.data.response);
                    this.scrollToBottom();
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },

            upload: function (id, file, index) {
                var $this = this;
                var form = new FormData();
                form.append('file', file);
                form.append('query', id);

                this.$http.post(URL.adminQuery.upload, form, {emulateJSON: true}).then(
                    function (response) {
                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                        $this.getPrivateQueries();
                        $this.queries[index].responses.push(response.data.response.response);
                        $this.scrollToBottom();
                    },
                    function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    }
                );
            },

            scrollToBottom: function () {
                $('#query-user-modal').animate({ scrollTop: $(document).height()}, 1000);
            }
        }
    });
}

