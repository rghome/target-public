'use strict';

Vue.config.debug = true;
Vue.config.devtools = true;

var favoritesVM = new Vue({
    el: '#favorites-vue',
    components: {
        notification: Notification
    },
    data: {
        form: '',
        show: false,
        favorites: [],
        sorting: 'DESC',
        busy: false,
        end : false,
        offset: 0,
        label: {
            header: messages.favorites.header,
            emptyPlaceholder: messages.favorites.emptyPlaceholder,
            added: messages.label.added,
            title: messages.label.title,
            button: messages.label.button,
            drop: messages.label.drop
        }
    },
    events: {
        createFavorite: function (id, e) {
            var el = $(e.target).parent();
            if (el.hasClass('non-active')) {
                this.create(id, el)
            } else if (el.hasClass('active')) {
                this.remove(id, el);
            }
        }
    },
    ready: function () {
        if (admin) {
            // this.getForm();
        }

        this.getFavorites();
        this.scrollLoad();
    },
    methods: {
        isFavorite: function (file_id, el) {
            var $this = this;
            var getUrl = URL.favorites.is + file_id;

            this.$http.get(getUrl).then(function (response) {
                var is = response.data.is;
                is.length ? $this.elActive(el) : $this.elNoneActive(el);
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        elActive: function(el) {
            el
                .addClass('active')
                .attr('title', 'Remove favorite')
                .removeClass('non-active')
            ;
        },
        elNoneActive: function (el) {
            el
                .addClass('non-active')
                .attr('title', 'Create favorite')
                .removeClass('active')
            ;
        },
        getFavorites: function () {
            var $this = this;
            var getUrl = URL.favorites.all+'?sorting=' + this.sorting + '&offset=' + this.offset;
            this.busy = true;
            // this.favorites = [];
            this.$http.get(getUrl).then(function (response) {
                var favorites = response.data.favorites;
                if (favorites.length == 0 ) {
                    this.end = true;
                    return ;
                }
                if (this.favorites.length > 0) {
                    favorites.forEach(function (item) {
                        $this.favorites.push(item);
                    });
                } else {
                    this.favorites = favorites;
                }
                this.busy = false;

                this.showPdf();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                this.busy = false;
            });
        },
        reload: function () {
            this.show = false;
            this.getFavorites();
        },
        create: function (id, el) {
            var $this = this;
            this.$http.get(URL.favorites.create + id).then(function (response) {
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                $this.favorites = [];
                $this.getFavorites();
                $this.elActive(el);
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        remove: function (id, el) {
            var $this = this;
            this.$http.get(URL.favorites.remove + id).then(function (response) {
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.favorites = [];
                this.getFavorites();

                if (el !== undefined) {
                    this.elNoneActive(el);
                } else {
                    var reportItem = $('#report-favs-' + id),
                        queryItem = $('#query-favs-' + id),
                        documentItem = $('#document-favs-' + id);

                    [reportItem, queryItem, documentItem].forEach(function (item) {
                        if (item.length) {
                            $this.elNoneActive(item);
                        }
                    });
                }

                // $('#favorite-item-' + id).parent('dd').remove();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        showPdf: function () {
            $("#pdf-head").text(this.favorites[0].file.name);
            PDFObject.embed('/' + this.favorites[0].file.path , "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },
        changeSorting: function () {
            this.sorting = this.sorting == 'DESC' ? 'ASC' : 'DESC';
            this.offset = 0;
            this.end = 0;
            this.favorites = [];
            this.getFavorites();
        },
        scrollLoad: function () {
            var $this = this;
            $(function () {
                var element = $('#favorites-vue .scroll-zone');
                element.scroll(function () {
                    var elHeight = element[0].scrollHeight - element.height();
                    if (elHeight - 10 <= element.scrollTop() && !$this.busy && !$this.end) {
                        $this.offset += 5;
                        $this.favorites = [];
                        $this.getFavorites();
                    }
                });
            });
        }
    }
});