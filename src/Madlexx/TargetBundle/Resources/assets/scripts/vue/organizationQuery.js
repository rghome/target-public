if (! admin) {
    var oqVM = new Vue({
        el: '#organization-query-vue',
        components: {
            notification: Notification,
        },
        data: {
            queries: [],
            user: 0,
            label: {
                header: messages.queries.headerOrganization,
                added: messages.label.added,
                submitted: messages.label.user,
                viewedByAdmin: messages.queries.viewedByAdmin,
                view: messages.queries.view,
                emptyPlaceholder: messages.queries.emptyPlaceholder,
                forPublication: messages.label.forPublication,
                name: messages.label.titleQuery,
                response: {
                    singular: messages.label.response,
                    create: messages.response.create
                }
            }
        },
        ready: function () {
            this.getQueries();
        },
        methods: {
            getQueries: function () {
                this.$http.get(URL.organizationQuery.all).then(function (response) {
                    var responseQueries = response.data.queries;
                    this.user = response.data.user;
                    this.queries = responseQueries;
                    if (responseQueries.length > 4) {
                        this.queries = responseQueries.slice(1).slice(-4);
                    }

                    setTimeout(function () {
                        $('#organization-queries-table').DataTable({
                            "info": false,
                            bFilter: false,
                            "bLengthChange": false,
                            "order": [[3, "desc"]],
                            "aoColumnDefs": [
                                {'bSortable': false, 'aTargets': [5]}
                            ]
                        });
                    }, 50);

                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            send: function (id, message, index) {
                this.$http.post(URL.adminQuery.send,
                    {
                        'id': id,
                        'message': message
                    }, {emulateJSON: true}).then(function (response) {
                    this.queries[index].responses.push(response.data.response);
                    this.scrollToBottom();
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            scrollToBottom: function () {
                $('#query-user-modal').animate({ scrollTop: $(document).height()}, 1000);
            },
            upload: function (id, file, index) {
                var $this = this;
                var form = new FormData();
                form.append('file', file);
                form.append('query', id);

                this.$http.post(URL.adminQuery.upload, form, {emulateJSON: true}).then(
                    function (response) {
                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                        $this.getQueries();
                        $this.queries[index].responses.push(response.data.response.response);
                        $this.scrollToBottom();
                    },
                    function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    }
                );
            },
        }
    });
}
