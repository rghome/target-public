'use strict';
Vue.config.debug = true;
Vue.config.devtools = true;

$(function () {
    if (! admin) {
        var chat = new Vue({
            el: '#chat-vue',
            components: {
                notification: Notification
            },
            data: {
                messages: [],
                message: {},
                count: 0,
                user: 0,
                chat: 0
            },
            ready: function () {
                this.getMessages();
            },
            methods: {
                getMessages: function () {
                    this.$http.get(chatUrl.get).then(function (response) {
                        this.count = +response.data.count;
                        this.messages = response.data.messages;
                        this.user = response.data.user;
                        this.chat = response.data.chat;
                    }, function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    });
                },
                send: function (message) {
                    if (message.length < 3) {
                        Methods.showErrorMessage(messages.error, 'Message cannot be less than 3.');
                        return;
                    }
                    this.$http.post(chatUrl.create, {
                        'chat': this.chat,
                        'message': message
                    }, {emulateJSON: true}).then(function (response) {
                        this.messages.unshift(response.data.message);
                        this.scrollToTop();
                    },function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    });
                },
                scrollToTop: function () {
                    var chatElement = $('.chat-message');
                    chatElement.animate({ scrollTop: 0 }, "slow");
                },
                clearCount: function () {
                    if (this.count > 0) {
                        this.$http.post(chatUrl.clear, {
                            'chat': this.chat,
                        }, {emulateJSON: true}).then(function (response) {
                            this.count = 0;
                        },function (response) {
                            Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                        });
                    }
                },
                upload: function (file) {
                    var form = new FormData();
                    form.append('file', file);
                    form.append('chat', this.chat);

                    this.$http.post(chatUrl.upload, form, {emulateJSON: true}).then(
                        function (response) {
                            Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                            this.getMessages();
                        },
                        function (response) {
                            Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                        }
                    );
                }
            }
        });

        if (!window.chat) {
            window.chat = chat;
        }
    }
});
