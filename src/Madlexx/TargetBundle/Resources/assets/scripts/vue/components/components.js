Vue.component('table-organizations', {
    template: '#vue-table-organizations',
    props: {
        organizations: Array,
        head: Object,
        actions: Object
    },
    methods: {
        remove: function (id, $event, $index) {
            if (vm.organizations.length <= 1) {
                Methods.showErrorMessage(messages.error, messages.last_organization);

                return;
            }

            this.$http.get(URL.delete + id).then(
                function (response) {
                    var tr = $($event.target).closest('tr');
                    tr.hide("slow", function () {
                        $(tr).remove()
                    })
                    vm.organizations.$remove(vm.organizations[$index]);
                    Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                },
                function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                }
            );
        },
        getEditForm: function (id, $event, $index) {
            vm.index = $index;
            vm.getEditForm(id);
        }
    }
});

Vue.component('table-directions', {
    template: '#vue-table-directions',
    props: {
        directions: Array,
        head: Object,
        actions: Object
    },
    methods: {
        remove: function (id, $event, $index) {
            if (vm.directions.length <= 1) {
                Methods.showErrorMessage(messages.error, messages.last_direction);

                return;
            }

            this.$http.get(URL.delete + id).then(
                function (response) {
                    var tr = $($event.target).closest('tr');
                    tr.hide("slow", function () {
                        $(tr).remove()
                    })
                    vm.directions.$remove(vm.directions[$index]);
                    Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                },
                function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                }
            );
        },
        getEditForm: function (id, $event, $index) {
            vm.index = $index;
            vm.getEditForm(id);
        }
    }
});

Vue.component('table-users', {
    template: '#vue-table-users',
    props: {
        users: Array,
        head: Object,
        actions: Object
    },
    methods: {
        remove: function (id, $event, $index) {
            if (vm.users.length <= 1) {
                Methods.showErrorMessage(messages.error, messages.last_user);

                return;
            }

            this.$http.get(URL.delete + id).then(
                function (response) {
                    var tr = $($event.target).closest('tr');
                    tr.hide("slow", function () {
                        $(tr).remove()
                    })
                    vm.users.$remove(vm.users[$index]);
                    Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                },
                function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                }
            );
        },
        getEditForm: function (id, $event, $index) {
            vm.index = $index;
            vm.getEditForm(id);
        }
    }
});

Vue.component('table-faqs', {
    template: '#vue-table-faqs',
    props: {
        faqs: Array,
        head: Object,
        actions: Object
    },
    methods: {
        remove: function (id, $event, $index) {
            if (vm.faqs.length <= 1) {
                Methods.showErrorMessage(messages.error, messages.last_faq);

                return;
            }

            this.$http.get(URL.delete + id).then(
                function (response) {
                    var tr = $($event.target).closest('tr');
                    tr.hide("slow", function () {
                        $(tr).remove()
                    })
                    vm.faqs.$remove(vm.faqs[$index]);
                    Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                },
                function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                }
            );
        },
        goToEdit: function (id, $event) {
            window.location.href = URL.edit + id;
        }
    }
});

Vue.component('modal', {
    template: '#vue-modal',
    props: {
        show: {
            type: Boolean,
            default: false,

        },
        button: {
            type: String,
            default: ""
        },
        title: {
            type: String,
            default: "Title of the modal"
        },
        body: {
            type: String,
            default: "Modal body"
        },
        footer: {
            type: String,
            default: ""
        },
        user: {
            type: Number,
            default: 0
        }
    },
    methods: {
        toggleActive: function () {
            this.show = !this.show;
        }
    }
});

Vue.component('black-modal', {
    template: '#vue-black-modal',
    props: {
        show: {
            type: Boolean,
            required: true,
            twoWay: true,
            default: false
        },
        body: String,
    }
})

Vue.component('documents', {
    template: '#vue-documents',
    props: {
        label: Object,
        documents: Array,
        show: Boolean,
        form: String,
    },
    directives: {
        isfavorite: function (file_id) {
            favoritesVM.isFavorite(file_id, $(this.el));
        }
    },
    methods: {
        setFavorite: function (file_id, e) {
            documentsVM.setFavorite(file_id, e);
        },
        remove: function (id) {
            documentsVM.remove(id);
        }
    }
});

Vue.component('reports', {
    template: '#vue-reports',
    props: {
        label: Object,
        reports: Array,
        show: Boolean,
        form: String,
        sorting: String
    },
    directives: {
        isfavorite: function (file_id) {
            favoritesVM.isFavorite(file_id, $(this.el));
        }
    },
    methods: {
        setFavorite: function (file_id, e) {
            reportsVM.setFavorite(file_id, e);
        },
        remove: function (id) {
            reportsVM.remove(id);
        },
        showPdf: function (report, event) {
            mostViewsVM.create(report.file_id);

            if (event) {
                event.preventDefault();
            }

            $.each($(event.target).closest('div').children('dd'), function (key, value) {
                $(value).get(0).style.background = "transparent";
            });

            $(event.target).closest('dd').get(0).style.background="rgba(3, 169, 244, 0.2)";
            $("#pdf-head").text(report.name);
            PDFObject.embed('/' + report.path, "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },
        changeSorting: function () {
            reportsVM.changeSorting();
        },
        isDesc: function () {
            return this.sorting == 'DESC';
        }
    }
});

Vue.component('admin-reports', {
    template: '#vue-admin-reports',
    props: {
        label: Object,
        reports: Array,
        show: Boolean,
        form: String,
        sorting: String,
        organizations: Number
    },
    methods: {
        setFavorite: function (file_id, e) {
            adminReportsVM.setFavorite(file_id, e);
        },
        remove: function (id) {
            adminReportsVM.remove(id);
        },
        changeSorting: function () {
            adminReportsVM.changeSorting();
        },
        isDesc: function () {
            return this.sorting == 'DESC';
        }
    }
});

Vue.component('favorites', {
    template: '#vue-favorites',
    props: {
        label: Object,
        favorites: Array,
        show: Boolean,
        form: String,
        sorting: String
    },
    methods: {
        remove: function (id) {
            favoritesVM.remove(id);
        },
        showPdf: function (favorite, event) {
            mostViewsVM.create(favorite.id);
            if (event) {
                event.preventDefault();
            }

            $.each($(event.target).closest('div').children('dd'), function (key, value) {
                $(value).get(0).style.background = "transparent";
            });

            $(event.target).closest('dd').get(0).style.background="rgba(3, 169, 244, 0.2)";
            $("#pdf-head").text(favorite.name);
            PDFObject.embed('/' + favorite.path, "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },
        changeSorting: function () {
            favoritesVM.changeSorting();
        },
        isDesc: function () {
            return this.sorting == 'DESC';
        }
    }
});

Vue.component('most-views', {
    template: '#vue-most-views',
    props: {
        label: Object,
        content: Array,
        show: Boolean,
        form: String,
        sorting: String
    },
    methods: {
        showPdf: function (item, event) {
            mostViewsVM.create(item.id);
            if (event) {
                event.preventDefault();
            }

            $.each($(event.target).closest('div').children('dd'), function (key, value) {
                $(value).get(0).style.background = "transparent";
            });

            $(event.target).closest('dd').get(0).style.background="rgba(3, 169, 244, 0.2)";
            $("#pdf-head").text(item.name);
            PDFObject.embed('/' + item.path, "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },
        changeSorting: function () {
            mostViewsVM.changeSorting();
        },
        isDesc: function () {
            return this.sorting == 'DESC';
        }
    }
});

Vue.component('private-queries', {
    template: '#vue-private-queries',
    props: {
        label: Object,
        queries: Array,
        show: Boolean,
        form: String,
        user: Number,
    },
    methods: {
        modal: function (query, event, $index) {
            event.preventDefault();
            var queryModal = DashboardVue.$children[1];
            queryModal.setReference(pqVM);
            queryModal.setUser(this.user);
            queryModal.showModal(query, $index);
        }
    }
});

Vue.component('organization-queries', {
    template: '#vue-organization-queries',
    props: {
        label: Object,
        queries: Array,
        user: Number,
    },
    methods: {
        modal: function (query, event, $index) {
            event.preventDefault();
            var queryModal = DashboardVue.$children[1];
            queryModal.setReference(oqVM);
            queryModal.setUser(this.user);
            queryModal.showModal(query, $index);
        }
    }
});

Vue.component('shared-queries', {
    template: '#vue-shared-queries',
    data: function () {
        return {
            perPage: 10,
            current: 0
        }
    },
    props: {
        label: Object,
        queries: Array,
        user: Number,
    },
    computed: {
        total: function() {
            return Math.ceil(this.queries.length / this.perPage)
        }
    },
    methods: {
        modal: function (query, event, $index) {
            event.preventDefault();
            var queryModal = DashboardVue.$children[1];
            queryModal.setUser(this.user);
            queryModal.setReference(sqVM);
            queryModal.showModal(query, $index);
        },
        show: function (event, page) {
            event.preventDefault();
            console.log(page);
        },
        setPage: function (n, event) {
            event.preventDefault();
            this.current = n;
        },
    },
    filters: {
        paginate: function(list) {
            if (this.current >= this.total) {
                this.current = this.total - 1
            }
            var index = this.current * this.perPage;
            if (index < 0) {
                index = 0;
            }
            return list.slice(index, index + this.perPage);
        }
    }
});

Vue.component('admin-queries', {
    template: '#vue-admin-queries',
    props: {
        label: Object,
        queries: Array,
        query: Object,
        response: Array,
        form: String,
        show: Boolean,
        user: Number,
        reference: 'admin'
    },
    methods: {
        create: function (id) {
            aqVM.getForm(id);
            this.show = true;
        }
    }
});

Vue.component('waiting-response', {
    template: '#vue-waiting-response',
    props: {
        label: Object,
        queries: Array,
        query: Object,
        response: Array,
        form: String,
        show: Boolean,
        user: Number,
        reference: {
            default: 'waiting'
        }
    },
    methods: {
        create: function (id, event) {
            event.preventDefault();
            wqAdmin.getForm(id);
            this.show = true;
        }
    }
});

Vue.component('response-modal', {
    template: '#vue-response-modal',
    props: {
        show: {
            type: Boolean,
            required: true,
            twoWay: true,
            default: false
        },
        body: String,
        response: {
            type: Array,
            twoWay: true,
        },
        query: {
            type: Object,
            twoWay: true,
        },
        user: {
            type: Number,
            twoWay: true,
        },
        reference: {
            default: 'admin'
        }
    },
    methods: {
        close: function () {
            this.show = !this.show;

            if (this.reference == 'admin') {
                wqAdmin.reload();
            } else {
                aqVM.reload();
            }
        },
        send: function (id, $event) {
            var element = $($event.target);
            var message = element.val();
            if (message.length < 3) {
                Methods.showErrorMessage(messages.error, 'Message cannot be less than 3.');
                return;
            }

            if (admin && this.reference == 'admin') {
                aqVM.send(id, message);
            }
            if (admin && this.reference == 'waiting') {
                wqAdmin.send(id, message);
            }

            element.val('');
        },
        isMessageOwner: function (user) {
            return user == this.user;
        },
        onChange: function (id, event) {
            var file = event.target.files[0];
            if (admin && this.reference == 'admin') {
                aqVM.upload(id, file);
            }
            if (admin && this.reference == 'waiting') {
                wqAdmin.upload(id, file);
            }
        }
    }
})

Vue.component('query-modal', {
    template: '#vue-query-modal',
    data: function () {
        return {
            show: false,
            query: {},
            body: '',
            index: '',
            reference: '',
            user: 0,
            label: {
                added: messages.label.added
            }
        };
    },
    directives: {
        isfavorite: function (file_id) {
            favoritesVM.isFavorite(file_id, $(this.el));
        }
    },
    methods: {
        setFavorite: function (file_id, e) {
            pqVM.setFavorite(file_id, e);
        },
        setReference: function (reference) {
            this.reference = reference;
        },
        setUser: function (user) {
            this.user = user;
        },
        close: function () {
            this.show = false;
            $('#pdf').html('Choose Response');
        },
        showModal: function (query, $index) {
            this.show = true;
            this.index = $index;
            this.query = query;
        },
        openResponse: function (webPath, event) {
            event.preventDefault();
            PDFObject.embed('/' + webPath, "#pdf", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },
        send: function (id, $event) {
            var element = $($event.target);
            var message = element.val();
            if (message.length < 3) {
                Methods.showErrorMessage(messages.error, 'Message cannot be less than 3.');
                return;
            }
            this.reference.send(id, message, this.index);

            element.val('');
        },
        isMessageOwner: function (user) {
            return user == this.user;
        },
        onChange: function (id, event) {
            var file = event.target.files[0];
            this.reference.upload(id, file, this.index);
        }
    }
})

Vue.component('chat', {
    template: '#vue-chat',
    props: {
        messages: Array,
        message: Object,
        show: Boolean,
        count: Number,
        user: Number
    },

    methods: {
        toggleActive: function () {
            this.show = !this.show;
            if (this.count > 0) {
                chat.clearCount();
            }
        },
        isMessageOwner: function (user) {
            return user == this.user;
        },
        send: function ($event) {
            var element = $($event.target);
            var message = element.val();
            element.val('');
            chat.send(message);
        },
        onChange: function (event) {
            var file = event.target.files[0];
            chat.upload(file);
        }
    }
});

Vue.component('chat-admin', {
    template: '#vue-chat-admin',
    props: {
        messages: Array,
        message: Object,
        chats: Array,
        user: Number,
        chat: Number,
        show: Boolean,
    },

    methods: {
        toggleActive: function () {
            this.show = !this.show;
            if (this.count > 0) {
                chatAdmin.clearCount();
            }
        },
        isMessageOwner: function (user) {
            return user == this.user;
        },
        openChat: function (id, item) {
            this.show = !this.show;
            chatAdmin.openChat(id, item.unread);
            item.unread = 0;
        },
        send: function ($event) {
            var element = $($event.target);
            var message = element.val();
            element.val('');
            chatAdmin.send(message);
        },
        onChange: function (event) {
            var file = event.target.files[0];
            chatAdmin.upload(file);
        }
    }
});

// Notify
const NotificationStore = {
    state: [], // here the notifications will be added

    addNotification: function (notification) {
        this.state.push(notification)
    },
    removeNotification: function (notification) {
        this.state.$remove(notification)
    }
}

const Methods = {
    showSuccessMessage: function (title, text) {
        NotificationStore.addNotification({
            title: title,
            text: text,
            type: "success",
            icon: "check",
            timeout: true
        })
    },
    showErrorMessage: function (title, text) {
        NotificationStore.addNotification({
            title: title,
            text: text,
            type: "danger",
            icon: "error_outline",
            timeout: true
        })
    },

    getMessage: function (response) {
        return response.data.message;
    },
};

Vue.component('notification', {
    template: '#notification',
    props: ['notification'],
    data: function () {
        return {timer: null}
    },
    ready: function () {
        var timeout = this.notification.hasOwnProperty('timeout') ? this.notification.timeout : true
        if (timeout) {
            var delay = this.notification.delay || 3000
            this.timer = setTimeout(function () {
                this.triggerClose(this.notification)
            }.bind(this), delay)
        }
    },

    methods: {
        triggerClose: function (notification) {
            clearTimeout(this.timer)
            this.$dispatch('close-notification', notification)
        }
    }
});

Vue.component('notifications', {
    template: '#notifications',
    data: function () {
        return {
            notifications: NotificationStore.state
        }
    },
    methods: {
        removeNotification: function (notification) {
            NotificationStore.removeNotification(notification)
        }
    }
});


Vue.directive("dropzone", {
    bind: function () {
        var idCapitalized = this.capitilize($(this.el).attr('id'));
        Dropzone.options[idCapitalized] = {
            addRemoveLinks: false,
            maxFilesize: 15,
            uploadMultiple: false,
            maxFiles: 1,
            clickable: true,
            acceptedFiles: ".pdf",
            dictCancelUpload: false,
            dictFallbackText: false
        };

        var $vC = this.vm.$children[0];
        var uploader = new Dropzone(this.el, {url: $(this.el).attr('action')});
        uploader.on('success', function (response) {
            this.removeAllFiles();
            $vC.show = false;
            aqVM.reload();
            documentsVM.reload();
            adminReportsVM.reload();
            Methods.showSuccessMessage('success', 'File successful uploaded!');
        });
    },
    capitilize: function ($string) {
        return $string.replace(/[-_]+(.)?/g, function (g) {
            return g[1].toUpperCase();
        })
    }
});

Vue.filter('truncate', function (text, stop, clamp) {
    return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
});