'use strict';

Vue.config.debug = true;
Vue.config.devtools = true;

var mostViewsVM = new Vue({
    el: '#most-views-vue',
    components: {
        notification: Notification
    },
    data: {
        form: '',
        show: false,
        content: [],
        sorting: 'DESC',
        busy: false,
        end : false,
        offset: 0,
        label: {
            header: messages.mostViews.header,
            emptyPlaceholder: messages.mostViews.emptyPlaceholder,
            added: messages.label.added,
            title: messages.label.title,
            button: messages.label.button,
            drop: messages.label.drop
        }
    },
    events: {
        // createFavorite: function (id, e) {
        //     var el = $(e.target).parent();
        //     if (el.hasClass('non-active')) {
        //         this.create(id, el)
        //     } else if (el.hasClass('active')) {
        //         this.remove(id, el);
        //     }
        // }
    },
    ready: function () {
        if (admin) {
            // this.getForm();
        }

        this.getMostViews();
        this.scrollLoad();
    },
    methods: {
        create: function (id) {
            var $this = this;
            this.$http.get(URL.mostViews.create + id).then(function (response) {
                $this.content = [];
                $this.getMostViews();
            });
        },
        getMostViews: function () {
            var $this = this;
            var getUrl = URL.mostViews.all+'?sorting=' + this.sorting + '&offset=' + this.offset;

            this.busy = true;
            // this.favorites = [];
            this.$http.get(getUrl).then(function (response) {
                var content = response.data.content;
                if (content.length == 0 ) {
                    this.end = true;
                    return ;
                }
                if (this.content.length > 0) {
                    content.forEach(function (item) {
                        $this.content.push(item);
                    });
                } else {
                    this.content = content;
                }
                this.busy = false;

                this.showPdf();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                this.busy = false;
            });
        },
        reload: function () {
            this.show = false;
            this.getMostViews();
        },
        changeSorting: function () {
            this.sorting = this.sorting == 'DESC' ? 'ASC' : 'DESC';
            this.offset = 0;
            this.end = 0;
            this.content = [];
            this.getMostViews();
        },
        scrollLoad: function () {
            var $this = this;
            $(function () {
                var element = $('#most-views-vue .scroll-zone');
                element.scroll(function () {
                    var elHeight = element[0].scrollHeight - element.height();
                    if (elHeight - 10 <= element.scrollTop() && !$this.busy && !$this.end) {
                        $this.offset += 5;
                        $this.content = [];
                        $this.getMostViews();
                    }
                });
            });
        },
        showPdf: function () {
            $("#pdf-head").text(this.content[0].file.name);
            PDFObject.embed('/' + this.content[0].file.path , "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },
    }
});