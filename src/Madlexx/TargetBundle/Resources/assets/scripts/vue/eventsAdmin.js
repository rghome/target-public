if (admin) {
    var ea = new Vue({
        el: '#vue-event-add',
        components: {
            notification: Notification,
        },
        data: {
            button: messages.events.create,
            title: messages.events.title,
            body: '',
            show: false,
        },
        ready: function () {
            this.getForm();
        },
        methods: {
            getForm: function () {
                this.$http.get(URL.events.form).then(function (response) {
                    this.body = response.data;
                    this.resetJsForm();
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });

            },
            create: function (e) {
                e.preventDefault();
                var data = $(e.target).serializeObject();

                this.$http.post(URL.events.create, data, {emulateJSON: true}).then(function (response) {
                    Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                    this.body = '';
                    this.getForm();
                    this.show = false;
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            resetJsForm: function () {
                setTimeout(function () {
                    $.material.init();
                    $('#event_date').datetimepicker({
                        format: 'd.m.Y g:i a',
                        hours12: true,
                        lang: 'en',
                    });
                    $('#madlexx_event').on('submit', function (e) {
                        ea.create(e);
                    })
                }, 200);
            }
        }
    });
}
