if (! admin) {
    var sqVM = new Vue({
        el: '#shared-query-vue',
        components: {
            notification: Notification,
        },
        data: {
            queries: [],
            user: 0,
            label: {
                submitted: messages.label.user,
                header: messages.queries.header,
                viewedByAdmin: messages.queries.viewedByAdmin,
                view: messages.queries.view,
                emptyPlaceholder: messages.queries.emptyPlaceholder,
                forPublication: messages.label.forPublication,
                added: messages.label.added,
                name: messages.label.titleQuery,
                title: messages.queries.title,
                button: messages.queries.create,
                response: {
                    singular: messages.label.response,
                    create: messages.response.create
                }
            }
        },
        ready: function () {
            this.getQueries();
        },
        methods: {
            getQueries: function () {
                this.$http.get(URL.sharedQuery.all).then(function (response) {
                    var responseQueries = response.data.queries;
                    this.queries = responseQueries;
                    this.user = response.data.user;

                    setTimeout(function () {
                        $('#shared-queries-table').DataTable({
                            "info": false,
                            bFilter: false,
                            "bLengthChange": false,
                            "order": [[3, "desc"]],
                            "aoColumnDefs": [
                                {'bSortable': false, 'aTargets': [5]}
                            ]
                        });
                    }, 50);
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            send: function (id, message, index) {
                this.$http.post(URL.adminQuery.send,
                    {
                        'id': id,
                        'message': message
                    }, {emulateJSON: true}).then(function (response) {
                    this.queries[index].responses.push(response.data.response);
                    this.scrollToBottom();
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },

            upload: function (id, file, index) {
                var $this = this;
                var form = new FormData();
                form.append('file', file);
                form.append('query', id);

                this.$http.post(URL.adminQuery.upload, form, {emulateJSON: true}).then(
                    function (response) {
                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                        $this.getQueries();
                        $this.queries[index].responses.push(response.data.response.response);
                        $this.scrollToBottom();
                    },
                    function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    }
                );
            },

            scrollToBottom: function () {
                $('#query-user-modal').animate({ scrollTop: $(document).height()}, 1000);
            }
        }
    });
}
