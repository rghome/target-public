if (admin) {
    var wqAdmin = new Vue({
        el: '#waiting-query-vue',
        components: {
            notification: Notification,
        },
        data: {
            form: '',
            show: false,
            query: {
                name: '',
                content: ''
            },
            queries: [],
            offset: 0,
            end: false,
            busy: false,
            user: 0,
            label: {
                header: messages.queries.pending,
                added: messages.label.added,
                submitted: messages.label.user,
                forPublication: messages.label.forPublication,
                query: messages.label.query,
                organization: messages.label.organization,
                actions: messages.label.actions,
                drop: messages.label.drop,
                response: {
                    singular: messages.label.response,
                    create: messages.response.create
                }
            }
        },
        ready: function () {
            this.getQueries();
            this.scrollLoad();
        },
        methods: {
            getQueries: function () {
                var $this = this;
                this.busy = true;
                var url = URL.adminQuery.not_responded + '?offset=' + this.offset;
                this.$http.get(url).then(function (response) {
                    response.data.queries.forEach(function (query, i) {
                        query.responses.forEach(function (responseSingle) {
                            if (responseSingle.belongsUser === true) {
                                --response.data.queries[i].response;
                            }
                        });
                    });

                    var queries = response.data.queries;
                    this.user = response.data.user;
                    if (queries.length == 0) {
                        this.end = true;
                        return ;
                    }
                    if (this.queries.length > 0) {
                        queries.forEach(function (item) {
                            $this.queries.push(item);
                        });
                    } else {
                        this.queries = queries;
                    }


                    this.busy = false;
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            getForm: function (id) {
                this.$http.get(URL.adminQuery.create + id).then(function (response) {
                    this.form = response.data.form;
                    this.response = response.data.response;
                    this.query = response.data.query;
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            scrollLoad: function () {
                var $this = this;
                $(function () {
                    var element = $('#waiting-query-vue .scroll-zone');
                    element.scroll(function () {
                        var elHeight = element[0].scrollHeight - element.height();
                        if (elHeight - 10 <= element.scrollTop() && !$this.busy && !$this.end) {
                            $this.offset += 5;
                            $this.getQueries();
                        }
                    });
                });
            },
            send: function (id, message) {
                this.$http.post(URL.adminQuery.send,
                    {
                        'id': id,
                        'message': message
                    }, {emulateJSON: true}).then(function (response) {
                    this.query.responses.push(response.data.response);

                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            upload: function (id, file) {
                var form = new FormData();
                form.append('file', file);
                form.append('query', id);

                this.$http.post(URL.adminQuery.upload, form, {emulateJSON: true}).then(
                    function (response) {
                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));

                        location.reload();
                    },
                    function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    }
                );
            },
            reload: function () {
                this.offset = 0;
                this.end = 0;
                this.getQueries();
            }
        }
    });
}