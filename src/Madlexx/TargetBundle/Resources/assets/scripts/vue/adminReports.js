'use strict';

Vue.config.debug = true;
Vue.config.devtools = true;

var adminReportsVM = new Vue({
    el: '#admin-reports-vue',
    components: {
        notification: Notification
    },
    data: {
        form: '',
        show: false,
        reports: [],
        sorting: 'DESC',
        organizations: 0,
        busy: false,
        end : false,
        offset: 0,
        label: {
            button: messages.label.button,
            drop: messages.label.drop,
            table: {
                date: messages.reports.table.date,
                name: messages.reports.table.name,
                allow: messages.reports.table.allow,
                availableOrganizations: messages.reports.table.availableOrganizations
            }
        }
    },
    ready: function () {
        if (admin) {
            this.getForm();
        }

        this.getReports();
        this.scrollLoad();
    },
    watch: {
        reports: function () {
            var $this = this;
            var table = $('#admin-reports-table');
            var organizationSelect = $('.organization-select');
            var organizationForm = $('form[name="report_organizations"]');

            $.fn.DataTable.ext.pager.numbers_length = 10;
            table.DataTable({
                "bSort":false,
                "bPaginate":true,
                // "sPaginationType":"full_numbers",
                "aLengthMenu": [[5, 10, 15, 25, 50, 100 , -1], [5, 10, 15, 25, 50, 100, "All"]],
                "iDisplayLength" : 5,
                "info": false,
                bFilter: false,
                "bLengthChange": false,
                "order": [[3, "desc"]]
            });

            table.off('page.dt');
            table.on('page.dt', function() {
                setTimeout(function() {
                    organizationSelect.select2();
                }, 50);
            });

            organizationSelect.off('change');
            organizationSelect.on('change', function() {
                $(this).parent('form').submit();
            });

            organizationForm.off('submit');
            organizationForm.on('submit', function (e) {
                e.preventDefault();

                var form = $(this),
                    url = URL.reports.setOrganizations + form.parent('td').parent('tr').data('id');

                $this.$http.post(url, form.serializeObject(), {emulateJSON: true})
                    .then(function (response) {
                        var row = $('#admin-reports-table').find("[data-id='" + response.data.report.id + "']");

                        this.organizations = response.data.organizationsCount;
                        if (response.data.report.organizationsCount == this.organizations) {
                            row.find('td:last').text('Yes');
                        } else {
                            row.find('td:last').text('No');
                        }

                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                    }), function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    };
            });

            organizationSelect.select2();
        }
    },
    methods: {
        getReports: function () {
            var $this = this;
            var getUrl = URL.reports.quarter+'?sorting=' + this.sorting + '&offset=' + this.offset;
            this.busy = true;
            this.$http.get(getUrl).then(function (response) {
                var reports = response.data.reports;
                if (reports.length == 0 ) {
                    this.end = true;
                    return ;
                }
                if (this.reports.length > 0) {
                    reports.forEach(function (item) {
                        $this.reports.push(item);
                    });
                } else {
                    this.reports = reports;
                }

                this.organizations = response.data.organizationsCount;
                this.busy = false;
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                this.busy = false;
            });
        },
        getForm: function () {
            this.$http.get(URL.reports.create).then(function (response) {
                this.form = response.data.form;
                setTimeout(function(){
                    var element = $('#report_file_form');
                    this.$compile(element.get(0));
                }.bind(this), 250);

            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        reload: function () {
            location.reload();
        },

        remove: function (id) {
            this.$http.get(URL.reports.remove + id).then(function (response) {
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.getReports();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },

        showPdf: function () {
            $("#pdf-head").text(this.reports[0].name);
            PDFObject.embed('/' + this.reports[0].path , "#pdf-view", {
                pdfOpenParams: {
                    zoom: 40,
                    toolbar: '1'
                }
            });
        },

        changeSorting: function () {
            this.sorting = this.sorting == 'DESC' ? 'ASC' : 'DESC';
            this.offset = 0;
            this.end = 0;
            this.reports = [];
            this.getReports();
        },

        scrollLoad: function () {
            var $this = this;
            $(function () {
                var element = $('#reports-vue .scroll-zone');
                element.scroll(function () {
                    var elHeight = element[0].scrollHeight - element.height();
                    if (elHeight - 10 <= element.scrollTop() && !$this.busy && !$this.end) {
                        $this.offset += 5;
                        $this.getReports();
                    }
                });
            });
        },
        setFavorite: function (file_id, e) {
            favoritesVM.$emit('createFavorite', file_id, e);
        }
    }
});