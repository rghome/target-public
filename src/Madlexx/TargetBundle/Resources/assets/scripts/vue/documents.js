Vue.config.debug = true;
Vue.config.devtools = true;

var documentsVM = new Vue({
    el: '#documents-vue',
    components: {
        notification: Notification
    },
    data: {
        form: '',
        show: false,
        documents: [],
        label: {
            header: messages.documents.header,
            emptyPlaceholder: messages.documents.emptyPlaceholder,
            added: messages.label.added,
            title: messages.label.title,
            button: messages.label.button,
            drop: messages.label.drop
        }
    },
    ready: function () {
        if (admin) {
            this.getForm();
        }

        this.getDocuments();
    },
    methods: {
        getDocuments: function () {
            this.$http.get(URL.documents.all).then(function (response) {
                var documents  = response.data.documents;

                this.documents = documents;
                if (documents.length > 4) {
                    this.documents = documents.slice(1).slice(-4);
                }
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        getForm: function () {
            this.$http.get(URL.documents.create).then(function (response) {
                this.form = response.data.form;
                setTimeout(function(){
                    var element = $('#document_file_form');
                    this.$compile(element.get(0));
                }.bind(this), 250);

            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        reload: function () {
            this.show = false;
            this.getDocuments();
        },
        remove: function (id) {
            this.$http.get(URL.documents.remove + id).then(function (response) {
                Methods.showSuccessMessage(messages.success, Methods.getMessage(response));
                this.getDocuments();
            }, function (response) {
                Methods.showErrorMessage(messages.error, Methods.getMessage(response));
            });
        },
        setFavorite: function (file_id, e) {
            favoritesVM.$emit('createFavorite', file_id, e);
        }
    }
});