if (admin) {
    var aqVM = new Vue({
        el: '#admin-query-vue',
        components: {
            notification: Notification,
        },
        data: {
            form: '',
            show: false,
            response: [],
            user: 0,
            query: {
                name: '',
                content: ''
            },
            queries: [],
            label: {
                header: messages.queries.all,
                added: messages.label.added,
                submitted: messages.label.user,
                forPublication: messages.label.forPublication,
                query: messages.label.query,
                organization: messages.label.organization,
                actions: messages.label.actions,
                drop: messages.label.drop,
                response: {
                    singular: messages.label.response,
                    create: messages.response.create
                }
            }
        },
        ready: function () {
            this.getQueries();
        },
        methods: {
            getQueries: function () {
                this.$http.get(URL.adminQuery.all).then(function (response) {
                    response.data.queries.forEach(function (query, i) {
                        query.responses.forEach(function (responseSingle) {
                            if (responseSingle.belongsUser === true) {
                                --response.data.queries[i].response;
                            }
                        });
                    });

                    this.queries = response.data.queries;
                    this.user = response.data.user;
                    setTimeout(function () {
                        $('#queries-table').DataTable({
                            "info": false,
                            bFilter: false,
                            "bLengthChange": false,
                            "order": [[3, "desc"]],
                            "aoColumnDefs": [
                                {'bSortable': false, 'aTargets': [5]}
                            ]
                        });
                    }, 50);
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            getForm: function (id) {
                this.$http.get(URL.adminQuery.create + id).then(function (response) {
                    this.form = response.data.form;
                    this.response = response.data.response;
                    this.query = response.data.query;
                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            send: function (id, message) {
                this.$http.post(URL.adminQuery.send,
                    {
                        'id': id,
                        'message': message
                    }, {emulateJSON: true}).then(function (response) {
                    this.query.responses.push(response.data.response);

                }, function (response) {
                    Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                });
            },
            upload: function (id, file) {
                var form = new FormData();
                form.append('file', file);
                form.append('query', id);

                this.$http.post(URL.adminQuery.upload, form, {emulateJSON: true}).then(
                    function (response) {
                        Methods.showSuccessMessage(messages.success, Methods.getMessage(response));

                        location.reload();
                    },
                    function (response) {
                        Methods.showErrorMessage(messages.error, Methods.getMessage(response));
                    }
                );
            },
            reload: function () {
                $('#queries-table').DataTable().destroy();
                this.getQueries();
            }
        }
    });
}