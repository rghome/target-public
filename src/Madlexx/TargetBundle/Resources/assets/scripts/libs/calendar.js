$(function(){
    var today = new Date();
    $('#calendar').eCalendar({
        ajaxDayLoader	: URL.events.days,
        ajaxEventLoader	: URL.events.events,
        eventsContainer	: "#events-list",
        currentMonth	: today.getMonth()+1,
        currentYear		: today.getFullYear(),
        startMonth		: 1,
        startYear		: today.getFullYear(),
        endMonth		: 12,
        endYear		: today.getFullYear() + 1,
        firstDayOfWeek	: 1,
        onBeforeLoad	: function() {},
        onAfterLoad		: function() {
            if (this.eventDays[0]) {
                $('[data-day='+this.eventDays[0]+']').click();
            }
        },
        onClickMonth	: function() {},
        onClickDay		: function() {}
    });
});
