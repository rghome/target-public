var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    exec = require('gulp-exec'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    gulpFilter = require('gulp-filter'),
    flatten = require('gulp-flatten'),
    minifycss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    stylus = require('gulp-stylus'),
    livereload = require('gulp-livereload');

var config = {
    src: {
        styl: './styl/main.styl',
        stylWatch: './styl/**/*.styl',
        js: './scripts/**/*.js',
        twig: '../views/**/*.html.twig'
    },
    dist: {
        css: '../public/css',
        js: '../public/scripts',
        bower: '../public/vendor',
        fonts: '/fonts'
    },
    path: '../../../../..'
};


var assetsInstall = function (err, stdout, stderr) {
    console.log(stdout);
};

gulp.task('styles', function(){
    gulp.src(config.src.styl)
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(stylus({
            compress: true
        }))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest(config.dist.css));
});

gulp.task('scripts', ['scripts-main', 'scripts-dashboard', 'scripts-users', 'scripts-organizations', 'scripts-directions', 'scripts-faqs']);

gulp.task('default', function(){
    gulp.watch(config.src.stylWatch, ['styles']);
    gulp.watch(config.src.js, ['scripts']);
});

gulp.task('scripts-main', function () {
    return gulp.src([
        './scripts/libs/dropzone.js',
        './scripts/material/jquery.min.js',
        './scripts/material/bootstrap.min.js',
        './scripts/libs/perfect-scrollbar.jquery.min.js',
        './scripts/material/material.min.js',
        './scripts/libs/jquery.dataTables.min.js',
        './scripts/libs/vue.js',
        './scripts/libs/vue-resource.min.js',
        './scripts/libs/vue-resource.min.js',
        './scripts/vue/components/components.js',
        './scripts/vue/chat.js',

    ])
        .pipe(jshint())
        .pipe(uglify())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(config.dist.js))
        .pipe(exec('php ' + config.path + '/bin/console assets:install ' + config.path + '/web/', assetsInstall));
})

gulp.task('scripts-dashboard', function () {
    return gulp.src([
        './scripts/vue/chatAdmin.js',
        './scripts/libs/pdfobject.min.js',
        './scripts/libs/jquery.event.calendar.js',
        './scripts/libs/jquery.event.calendar.en.js',
        './scripts/libs/calendar.js',
        './scripts/libs/select2.min.js',
        './scripts/vue/privateQuery.js',
        './scripts/vue/organizationQuery.js',
        './scripts/vue/sharedQuery.js',
        './scripts/vue/adminQuery.js',
        './scripts/vue/waitResponce.js',
        './scripts/vue/eventsAdmin.js',
        './scripts/libs/datapicker.js',
        './scripts/libs/dataTables.bootstrap.min.js',
        './scripts/vue/documents.js',
        './scripts/vue/reports.js',
        './scripts/vue/favorites.js',
        './scripts/vue/adminReports.js',
        './scripts/vue/mostViews.js',
        './scripts/vue/notifications.js',
    ])
        .pipe(jshint())
        .pipe(uglify())
        .pipe(concat('dashboard.js'))
        .pipe(gulp.dest(config.dist.js))
        .pipe(exec('php ' + config.path + '/bin/console assets:install ' + config.path + '/web/', assetsInstall));
})

gulp.task('scripts-users', function () {
    return gulp.src([
        './scripts/libs/select2.min.js',
        './scripts/vue/directives.js',
        './scripts/vue/users.js',

    ])
        .pipe(jshint())
        .pipe(uglify())
        .pipe(concat('users.js'))
        .pipe(gulp.dest(config.dist.js));
})

gulp.task('scripts-organizations', function () {
    return gulp.src([
        './scripts/libs/select2.min.js',
        './scripts/vue/directives.js',
        './scripts/vue/organizations.js',

    ])
        .pipe(jshint())
        .pipe(uglify())
        .pipe(concat('organizations.js'))
        .pipe(gulp.dest(config.dist.js))
        .pipe(exec('php ' + config.path + '/bin/console assets:install ' + config.path + '/web/', assetsInstall));
})

gulp.task('scripts-directions', function () {
    return gulp.src([
        './scripts/libs/select2.min.js',
        './scripts/vue/directives.js',
        './scripts/vue/directions.js',

    ])
        .pipe(jshint())
        .pipe(uglify())
        .pipe(concat('directions.js'))
        .pipe(gulp.dest(config.dist.js))
        .pipe(exec('php ' + config.path + '/bin/console assets:install ' + config.path + '/web/', assetsInstall));
})

gulp.task('scripts-faqs', function () {
    return gulp.src([
        './scripts/vue/faqs.js',

    ])
        .pipe(jshint())
        .pipe(uglify())
        .pipe(concat('faqs.js'))
        .pipe(gulp.dest(config.dist.js));
})

gulp.task('build', ['styles', 'scripts']);
