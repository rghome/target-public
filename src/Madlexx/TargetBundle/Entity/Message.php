<?php

namespace Madlexx\TargetBundle\Entity;

use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;

/**
 * Message
 */
class Message implements \JsonSerializable
{
    use DateTimeTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Chat
     */
    private $chat;

    /**
     * @var Direction
     */
    private $direction;

    /**
     * @var boolean
     */
    private $seen;

    /**
     * @var File
     */
    private $file;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Message
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Message
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set chat
     *
     * @param Chat $chat
     *
     * @return Message
     */
    public function setChat(Chat $chat = null)
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }

    /**
     * @return bool
     */
    public function isSeen()
    {
        return $this->seen;
    }

    /**
     * @param $seen
     *
     * @return $this
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get file
     *
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param File $file
     *
     * @return Response
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get direction
     *
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set direction
     *
     * @param Direction $direction
     *
     * @return $this
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'content' => $this->getContent(),
            'user'   => $this->getUser()->getId(),
            'path'  => $this->getFile() ? $this->getFile()->getWebPath() : '',
            'createdAt' => $this->getCreatedAt()->format('m/d/Y'),
            'direction' => $this->getDirection() ? $this->getDirection()->getName() : '',
        ];
    }
}
