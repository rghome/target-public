<?php

namespace Madlexx\TargetBundle\Entity;

use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;

/**
 * Document
 */
class Document implements \JsonSerializable
{
    use DateTimeTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var File
     */
    private $file;

    /**
     * @var Direction
     */
    private $direction;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set file
     *
     * @param File $file
     *
     * @return $this
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get direction
     *
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set direction
     *
     * @param Direction $direction
     *
     * @return $this
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'file_id' => $this->getFile() ? $this->getFile()->getId() : '',
            'path' => $this->getFile() ? $this->getFile()->getWebPath() : '',
            'added' => $this->getCreatedAt()->format('m/d/Y'),
            'direction' => $this->getDirection() ? $this->getDirection()->getName() : '',
        ];
    }
}
