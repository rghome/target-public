<?php

namespace Madlexx\TargetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;

/**
 * Query
 */
class Query implements \JsonSerializable
{
    use DateTimeTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $content;

    /**
     * @var boolean
     */
    private $confidential;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Collection
     */
    private $response;

    /**
     * @var Organization
     */
    private $organization;

    /**
     * @var boolean
     */
    private $viewed;

    /**
     * @var boolean
     */
    private $forPublication;

    /**
     * @var Direction
     */
    private $direction;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->response = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set confidential
     *
     * @param boolean $confidential
     *
     * @return $this
     */
    public function setConfidential($confidential)
    {
        $this->confidential = $confidential;

        return $this;
    }

    /**
     * Get confidential
     *
     * @return boolean
     */
    public function isConfidential()
    {
        return $this->confidential;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add response
     *
     * @param Response $response
     *
     * @return $this
     */
    public function addResponse(Response $response)
    {
        if (!$this->response->contains($response)) {
            $this->response[] = $response;
        }

        return $this;
    }

    /**
     * Remove response
     *
     * @param Response $response
     */
    public function removeResponse(Response $response)
    {
        if ($this->response->contains($response)) {
            $this->response->removeElement($response);
        }
    }

    /**
     * Get response
     *
     * @return Collection
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization = null)
    {
        $this->organization = $organization;
    }

    /**
     * Get direction
     *
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set direction
     *
     * @param Direction $direction
     *
     * @return $this
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'added' => $this->getCreatedAt()->format('m/d/Y'),
            'content'   => $this->getContent(),
            'response' => $this->countResponseFiles(),
            'responses' => $this->getResponse()->toArray(),
            'userName' => $this->getUser()->getUsername(),
            'organization' => $this->getOrganization() ? $this->getOrganization()->getName(): '',
            'direction' => $this->getDirection() ? $this->getDirection()->getName() : '',
            'confidential' => $this->isConfidential(),
            'viewed' => $this->isViewed(),
            'forPublication' => $this->isForPublication()
        ];
    }

    /**
     * @return int
     */
    public function countResponseFiles()
    {
        $count = 0;
        foreach ($this->getResponse() as $response) {
            if (!$response->isMessageType()) {
                $count ++;
            }
        }

        return $count;
    }
    /**
     * Get confidential
     *
     * @return boolean
     */
    public function getConfidential()
    {
        return $this->confidential;
    }

    /**
     * @return boolean
     */
    public function isViewed()
    {
        return $this->viewed;
    }

    /**
     * @param boolean $viewed
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    }

    /**
     * @return boolean
     */
    public function isForPublication()
    {
        return $this->forPublication;
    }

    /**
     * @param boolean $forPublication
     */
    public function setForPublication($forPublication)
    {
        $this->forPublication = $forPublication;
    }
}
