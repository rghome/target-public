<?php

namespace Madlexx\TargetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;
use Doctrine\Common\Collections\Collection;

/**
 * Report
 */
class Report implements \JsonSerializable
{
    use DateTimeTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var File
     */
    private $file;

    /**
     * @var Direction
     */
    private $direction;

    /**
     * @var Collection|Organization
     */
    private $organizations;

    /**
     * Report constructor.
     */
    public function __construct()
    {
        $this->organizations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set file
     *
     * @param File $file
     *
     * @return $this
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get direction
     *
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set direction
     *
     * @param Direction $direction
     *
     * @return $this
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @param Organization $organization
     */
    public function addOrganization(Organization $organization)
    {
        if ($this->organizations->contains($organization)) {
            return;
        }

        $this->organizations->add($organization);
        $organization->addReport($this);
    }

    /**
     * @param Organization $organization
     */
    public function removeOrganization(Organization $organization)
    {
        if ($this->organizations->contains($organization)) {
            return;
        }

        $this->organizations->removeElement($organization);
        $organization->removeReport($this);
    }

    /**
     * @return ArrayCollection|Collection|Organization
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'file_id' => $this->getFile()->getId(),
            'name' => $this->getName(),
            'path' => $this->getFile() ? $this->getFile()->getWebPath() : '',
            'added' => $this->getCreatedAt()->format('m/d/Y'),
            'direction' => $this->getDirection() ? $this->getDirection()->getName() : '',
            'organizations' => $this->getOrganizations()->toArray(),
            'organizationsCount' => count($this->getOrganizations()->toArray()),
        ];
    }
}
