<?php

namespace Madlexx\TargetBundle\Entity\Repository;

use Madlexx\TargetBundle\Entity\Repository\RepositoryTraits\SearchTrait;

/**
 * Class ResponseRepository
 *
 * @package Madlexx\TargetBundle\Entity\Repository
 */
class ResponseRepository
{
    use SearchTrait;
}
