<?php

namespace Madlexx\TargetBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ChatRepository
 *
 * @package Madlexx\TargetBundle\Entity\Repository
 */
class ChatRepository extends EntityRepository
{
    const LIMIT = 15;

    /**
     * @param $id
     *
     * @return array
     */
    public function getByUser($id)
    {
        $qb = $this->createQueryBuilder('chat');

        $qb->leftJoin('chat.user', 'user');
        $qb->leftJoin('chat.messages', 'messages');

        $qb->select('chat');
        $qb->addSelect('messages');

        $qb->where(
            $qb->expr()->eq('user.id', ':id')
        )->setParameter('id', $id);

        $qb->setMaxResults(self::LIMIT);

        $qb->orderBy('messages.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getByChat($id)
    {
        $qb = $this->createQueryBuilder('chat');

        $qb->leftJoin('chat.user', 'user');
        $qb->leftJoin('chat.messages', 'messages');

        $qb->select('chat');
        $qb->addSelect('messages');

        $qb->where(
            $qb->expr()->eq('chat.id', ':id')
        )->setParameter('id', $id);

        $qb->setMaxResults(self::LIMIT);

        $qb->orderBy('messages.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
    /**
     * @param int $id
     *
     * @param int $userId
     *
     * @return mixed
     */
    public function countUnread($id, $userId)
    {
        $qb = $this->createQueryBuilder('chat');

        $qb->leftJoin('chat.messages', 'messages');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('chat.id', ':id'),
                $qb->expr()->eq('messages.seen', ':seen'),
                $qb->expr()->neq('messages.user', ':user')
            )
        )->setParameters([
            'id' => $id,
            'seen' => false,
            'user' => $userId
        ]);

        $qb->select(
            $qb->expr()->count('messages.id')
        );

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $id
     * @param int $userId
     *
     * @return array
     */
    public function getUnread($id, $userId)
    {
        $qb = $this->createQueryBuilder('chat');

        $qb->leftJoin('chat.messages', 'messages', 'WITH', 'messages.chat = :chat');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('chat.id', ':id'),
                $qb->expr()->eq('messages.seen', ':seen'),
                $qb->expr()->neq('messages.user', ':user')
            )
        )->setParameters([
            'id' => $id,
            'chat' => $id,
            'seen' => false,
            'user' => $userId
        ]);

        $qb->addSelect('messages');

        return $qb->getQuery()->getSingleResult();
    }
}
