<?php

namespace Madlexx\TargetBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class EventRepository
 *
 * @package Madlexx\TargetBundle\Entity\Repository
 */
class EventRepository extends EntityRepository
{
    /**
     * @param string $month
     * @param int   $day
     * @param User $user
     *
     * @return array
     */
    public function getEventMonth($month = 'm', $day = null, User $user)
    {
        $start = 1;
        $end = 31;
        if (!is_null($day)) {
            $start = $end = $day;
        }

        $startDate = date("Y-{$month}-{$start}");
        $endDate = date("Y-{$month}-{$end}");

        $qb =$this->createQueryBuilder('event');

        $qb->select('event');

        if (is_null($day)) {
            $expr = $qb->expr()->andX(
                $qb->expr()->gte('event.date', ':start'),
                $qb->expr()->lte('event.date', ':end')
            );
            $params = [
                'start' => $startDate,
                'end' => $endDate
            ];
        } else {
            $expr = $qb->expr()->eq('DATE(event.date)', ':start');
            $params = [
                'start' => $startDate,
            ];
        }
        $qb->where(
            $expr
        );

        if ($user->getDirection()) {
            $qb->andWhere(
                $qb->expr()->eq('event.direction', ':directionId')
            );
            $params['directionId'] = $user->getDirection()->getId();
        }

        $qb->setParameters($params);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $month
     * @param User $user
     *
     * @return array
     */
    public function getDays($month = '', User $user)
    {
        if (empty($month)) {
            $month = 'm';
        }

        $startDate = date("Y-{$month}-1");
        $endDate = date("Y-{$month}-31");

        $qb =$this->createQueryBuilder('event');

        $qb->select('DAY(event.date) as day');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->gte('event.date', ':start'),
                $qb->expr()->lte('event.date', ':end')
            )
        );
        $params = [
            'start' => $startDate,
            'end' => $endDate
        ];

        if ($user->getDirection()) {
            $qb->andWhere(
                $qb->expr()->eq('event.direction', ':directionId')
            );
            $params['directionId'] = $user->getDirection()->getId();
        }

        $qb->setParameters($params);

        return $qb->getQuery()->getScalarResult();
    }
}
