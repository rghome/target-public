<?php

namespace Madlexx\TargetBundle\Entity\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\FOSUserBundle\Entity\User;
/**
 * Class QueryRepository
 *
 * @package Madlexx\TargetBundle\Entity\Repository
 */
class QueryRepository extends EntityRepository
{
    /**
     * @param int $limit
     * @param int $offset
     * @param User $manager
     *
     * @return array
     */
    public function getLastNotResponded($offset = 0, $limit = 5, User $manager)
    {
        $qb = $this->createQueryBuilder('q');
        $sqb = $this->createQueryBuilder('sq');
        $sqb
            ->select('sq.id')
            ->distinct()
            ->leftJoin('sq.response', 'response')
            ->where(
                $sqb->expr()->isNotNull('response.file')
            )
            ->andWhere(
                $sqb->expr()->eq('response.belongsUser', ':belongsUser')
            );

        $qb->setParameter('belongsUser', 0);

        $qb->andWhere(
            $qb->expr()
                ->notIn('q.id', $sqb->getDQL())
        )
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        if ($manager->getDirection()) {
            $qb->andWhere('q.direction = :directionId')
                ->setParameter('directionId', $manager->getDirection()->getId());
        }


        return $qb->getQuery()->getResult();
    }

    /**
     * @param Organization $organization
     * @param null $orderBy
     *
     * @return array
     */
    public function findMoreThenOrganizationCreated(
        Organization $organization,
        $orderBy = null//, $limit = null, $offset = null
    )
    {
        $qb = $this->createQueryBuilder('q');

        $qb->where('q.createdAt > :target')
           ->andWhere('q.confidential = false')
           ->andWhere('q.direction = :directionId')
           ->orderBy('q.id', $orderBy)
//           ->setFirstResult( $offset )
//           ->setMaxResults( $limit )
           ->setParameter('target', $organization->getCreatedAt(), Type::DATETIME)
           ->setParameter('directionId', $organization->getDirection()->getId());

        return $qb->getQuery()->getResult();
    }
}
