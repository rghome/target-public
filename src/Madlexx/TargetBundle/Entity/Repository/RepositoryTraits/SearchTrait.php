<?php

namespace Madlexx\TargetBundle\Entity\Repository\RepositoryTraits;

/**
 * Class SearchTrait
 *
 * @package Madlexx\TargetBundle\Entity\Repository\RepositoryTraits
 */
trait SearchTrait
{
    /**
     * @param $needle
     *
     * @return array
     */
    public function search($needle = '')
    {
        $qb = $this->createQueryBuilder('e');

        $qb->select('e');
        $qb->leftJoin('e.file', 'file');
        $qb->addSelect('file');

        foreach (explode(' ', $needle) as $key => $term) {
            $qb->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->like('e.name', ':name'.$key)
                )
            )->setParameter('name'.$key, '%'.$term.'%');
        }

        return $qb->getQuery()->getResult();
    }
}
