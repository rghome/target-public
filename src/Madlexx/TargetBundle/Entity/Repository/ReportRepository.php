<?php

namespace Madlexx\TargetBundle\Entity\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\TargetBundle\Entity\Repository\RepositoryTraits\SearchTrait;

/**
 * Class ReportRepository
 *
 * @package Madlexx\TargetBundle\Entity\Repository
 */
class ReportRepository extends EntityRepository
{
    use SearchTrait;

    /**
     * @param Organization $organization
     * @param null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @return array
     */
    public function findMoreThenOrganizationCreated(
        Organization $organization,
        $orderBy = null, $limit = null, $offset = null
    )
    {
        $qb = $this->createQueryBuilder('r');

        $qb->where('r.createdAt > :target')
            ->andWhere('r.direction = :directionId')
            ->orderBy('r.id', $orderBy)
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
            ->setParameter('target', $organization->getCreatedAt(), Type::DATETIME)
            ->setParameter('directionId', $organization->getDirection()->getId());

        return $qb->getQuery()->getResult();
    }
}
