<?php

namespace Madlexx\TargetBundle\Entity\EntityTrait;

trait DateTimeTrait
{
    /**
     * @return $this
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * @return $this
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }
}
