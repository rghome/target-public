<?php

namespace Madlexx\TargetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Madlexx\FOSUserBundle\Entity\Direction;
use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\TargetBundle\Entity\EntityTrait\DateTimeTrait;

/**
 * Response
 */
class Response implements \JsonSerializable
{
    use DateTimeTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $content;

    /**
     * @var bool
     */
    private $confidential;

    /**
     * @var bool
     */
    private $belongsUser;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Query
     */
    private $query;

    /**
     * @var File
     */
    private $file;

    /**
     * @var Direction
     */
    private $direction;

    /**
     * @var bool
     */
    private $messageType;

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get confidential
     *
     * @return bool
     */
    public function isConfidential()
    {
        return $this->confidential;
    }

    /**
     * Get confidential
     *
     * @return bool
     */
    public function getConfidential()
    {
        return $this->confidential;
    }

    /**
     * Set confidential
     *
     * @param bool $confidential
     *
     * @return $this
     */
    public function setConfidential($confidential)
    {
        $this->confidential = $confidential;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isBelongsUser()
    {
        return $this->belongsUser;
    }

    /**
     * @param boolean $belongsUser
     */
    public function setBelongsUser($belongsUser)
    {
        $this->belongsUser = $belongsUser;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get query
     *
     * @return Query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set query
     *
     * @param Query $query
     *
     * @return $this
     */
    public function setQuery(Query $query = null)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get file
     *
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param File $file
     *
     * @return Response
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMessageType()
    {
        return $this->messageType;
    }

    /**
     * @param $message
     *
     * @return $this
     */
    public function setMessageType($message)
    {
        $this->messageType = $message;

        return $this;
    }

    /**
     * Get direction
     *
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set direction
     *
     * @param Direction $direction
     *
     * @return $this
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id'    => $this->getId(),
            'name'  => $this->getName(),
            'added' => $this->getCreatedAt()->format('m/d/Y'),
            'path'  => $this->getFile() ? $this->getFile()->getWebPath() : '',
            'file_id' => $this->getFile() ? $this->getFile()->getId() : '',
            'content' => $this->getContent(),
            'user' => $this->getUser() ? $this->getUser()->getId() : '',
            'messageType' => $this->isMessageType(),
            'createdAt' => $this->getCreatedAt()->format('m/d/Y'),
            'direction' => $this->getDirection() ? $this->getDirection()->getName() : '',
            'belongsUser' => $this->isBelongsUser()
        ];
    }
}
