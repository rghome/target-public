<?php

namespace Madlexx\TargetBundle\Form\Handler;

use Doctrine\ORM\EntityManager;

use Madlexx\TargetBundle\Entity\Chat;
use Madlexx\TargetBundle\Entity\Message;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

use Madlexx\TargetBundle\Entity\Document;
use Madlexx\TargetBundle\Entity\File;
use Madlexx\TargetBundle\Entity\Report;
use Madlexx\TargetBundle\Entity\Query;
use Madlexx\TargetBundle\Entity\Response;
use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\TargetBundle\Handler\EmailHandler;

/**
 * Class FileHandler
 *
 * @package Madlexx\TargetBundle\Form\Handler
 */
class FileHandler
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var string
     */
    protected $absPath;

    /**
     * @var string
     */
    protected $webPath;

    /**
     * @var EmailHandler
     */
    protected $emailHandler;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * FileHandler constructor.
     *
     * @param EntityManager $entityManager
     * @param               $rootDir
     * @param               $webPath
     */
    public function __construct(
        EntityManager $entityManager,
        $rootDir,
        $webPath,
        AuthorizationChecker $authorizationChecker
    ) {
        $this->em = $entityManager;
        $this->rootDir = $rootDir;
        $this->webPath = $webPath;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param Form    $form
     * @param Request $request
     * @param File    $file
     * @param User    $manager
     *
     * @return array
     */
    public function handleDocument(Form &$form, Request &$request, File &$file, User $manager)
    {
        $success = false;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploaded = $request->files->get('file');
            $document = new Document();
            $file->setName($uploaded->getClientOriginalName());
            $file->setSize($uploaded->getSize());
            $document->setName($file->getName());
            $document->setFile($file);
            $fileName = $this->fileUpload($request->files->get('file'));
            $file->setWebPath($this->webPath . $fileName);
            $file->setAbsPath($this->getAbsPath() . $fileName);

            if ($manager->getDirection()) {
                $document->setDirection($manager->getDirection());
            }

            $this->em->persist($document);
            $this->em->flush();

            $success = true;
        }

        return compact('success');
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    protected function fileUpload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessClientExtension();
        $file->move($this->getAbsPath(), $fileName);

        return $fileName;
    }

    /**
     * @return string
     */
    private function getAbsPath()
    {
        if (null === $this->absPath) {
            $this->absPath = $this->rootDir . $this->webPath ;
        }

        return $this->absPath;
    }

    /**
     * @param Form    $form
     * @param Request $request
     * @param File    $file
     * @param User    $manager
     *
     * @return array
     */
    public function handleReport(Form &$form, Request &$request, File &$file, User $manager)
    {
        $success = false;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploaded = $request->files->get('file');
            $report = new Report();
            $file->setName($uploaded->getClientOriginalName());
            $file->setSize($uploaded->getSize());
            $report->setName($file->getName());
            $report->setFile($file);
            $fileName = $this->fileUpload($request->files->get('file'));
            $file->setWebPath($this->webPath . $fileName);
            $file->setAbsPath($this->getAbsPath() . $fileName);

            if ($manager->getDirection()) {
                $report->setDirection($manager->getDirection());
            }

            if ($manager->getOrganization()) {
                $report->addOrganization($manager->getOrganization());
            }

            $this->em->persist($report);
            $this->em->flush();

            $success = true;
        }

        return compact('success');
    }

    /**
     * @param Form    $form
     * @param Request $request
     * @param File    $file
     *
     * @param Query   $query
     *
     * @param User    $user
     *
     * @return array
     */
    public function handleResponse(Form &$form, Request &$request, File &$file, Query $query, User $user)
    {
        $success = false;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploaded = $request->files->get('file');
            $response = new Response();
            $file->setName($uploaded->getClientOriginalName());
            $file->setSize($uploaded->getSize());
            $response->setName($file->getName());
            $response->setFile($file);
            $response->setUser($user);
            $response->setMessageType(false);
            $response->setQuery($query);
            $fileName = $this->fileUpload($request->files->get('file'));
            $file->setWebPath($this->webPath . $fileName);
            $file->setAbsPath($this->getAbsPath() . $fileName);

            if ($user->getDirection()) {
                $response->setDirection($user->getDirection());
            }

            $this->em->persist($response);
            $this->em->flush();

            $this->emailHandler->sendNotification($query->getUser(), $response->getName(), $query->getName());

            $success = true;
        }

        return compact('response', 'success');
    }

    /**
     * @param File $file
     *
     * @return bool
     * @throws \Exception
     */
    public function removeFile(File $file)
    {
        if ($file->getAbsPath()) {
            try {
                unlink($file->getAbsPath());
            } catch (\Exception $e) {
                throw $e;
            }
        }

        return true;
    }

    /**
     * @param EmailHandler $emailHandler
     *
     * @return FileHandler
     */
    public function setEmailHandler(EmailHandler $emailHandler)
    {
        $this->emailHandler = $emailHandler;

        return $this;
    }

    /**
     * @param Request $request
     * @param User    $user
     *
     * @return bool
     */
    public function handleChatUpload(Request $request, User $user)
    {
        $uploaded = $request->files->get('file');
        if (!$uploaded) {
            return false;
        }
        $file = new File();
        $message = new Message();
        $chat = $this->em->getRepository(Chat::class)->findOneBy(['id' => $request->request->get('chat')]);
        $file->setName($uploaded->getClientOriginalName());
        $file->setSize($uploaded->getSize());
        $message->setContent($file->getName());
        $message->setFile($file);
        $message->setUser($user);
        $message->setSeen(false);
        $message->setChat($chat);

        $fileName = $this->fileUpload($request->files->get('file'));
        $file->setWebPath($this->webPath . $fileName);
        $file->setAbsPath($this->getAbsPath() . $fileName);

        $this->em->persist($message);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param User $user
     * @param Query $query
     *
     * @return array|bool
     */
    public function handleResponseUpload(Request $request, User $user, Query $query)
    {
        $uploaded = $request->files->get('file');
        if (!$uploaded) {
            return false;
        }
        $file = new File();

        $response = new Response();
        $file->setName($uploaded->getClientOriginalName());
        $file->setSize($uploaded->getSize());
        $response->setName($file->getName());
        $response->setFile($file);
        $response->setUser($user);
        $response->setMessageType(false);
        $response->setQuery($query);

        if ($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN') ||
            $this->authorizationChecker->isGranted('ROLE_ADMIN')
        ) {
            $response->setBelongsUser(false);
        } else {
            $response->setBelongsUser(true);
        }

        $fileName = $this->fileUpload($request->files->get('file'));
        $file->setWebPath($this->webPath . $fileName);
        $file->setAbsPath($this->getAbsPath() . $fileName);

        if ($user->getDirection()) {
            $response->setDirection($user->getDirection());
        }

        $this->em->persist($response);
        $this->em->flush();

        return compact('response');
    }
}
