<?php

namespace Madlexx\TargetBundle\Form\Handler;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

use Madlexx\TargetBundle\Entity\Faq;

/**
 * Class FaqHandler
 *
 * @package Madlexx\TargetBundle\Form\Handler
 */
class FaqHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Form    $form
     * @param Request $request
     * @param Faq     $faq
     *
     * @return array
     */
    public function handle(Form &$form, Request &$request, Faq $faq)
    {
        $success = false;

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $this->entityManager->persist($faq);
            $this->entityManager->flush();
            $success = true;
        }

        return [
            'form' => $form->createView(),
            'success' => $success
        ];
    }
}
