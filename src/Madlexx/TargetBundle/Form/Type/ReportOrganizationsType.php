<?php


namespace Madlexx\TargetBundle\Form\Type;


use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\FOSUserBundle\Entity\Repository\OrganizationRepository;
use Madlexx\FOSUserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\TargetBundle\Entity\Report;

/**
 * Class ReportOrganizationsType
 * @package Madlexx\TargetBundle\Form\Type
 */
class ReportOrganizationsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $manager = $options['manager'];

        $builder
            ->add(
                'organizations',
                EntityType::class,
                [
                    'label'         => false,
                    'required'      => false,
                    'class'         => Organization::class,
                    'query_builder' => function (OrganizationRepository $directionRepository) use ($manager) {
                        $qb = $directionRepository->createQueryBuilder('organization');

                        if (!in_array(User::ROLE_SUPER_ADMIN, $manager->getRoles())) {
                            $qb->where(
                                $qb->expr()->eq('organization.direction', ':direction')
                            )->setParameter('direction', $manager->getDirection()->getId());
                        }

                        return $qb;
                    },
                    'choice_value' => 'id',
                    'choice_label'  => 'name',
                    'multiple'      => true,
                    'by_reference'  => true,
                    'attr'          => [
                        'placeholder' => false,
                        'class'       => 'organization-select'
                    ],
                ]
            )
        ;
    }



    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Report::class);
        $resolver->setDefault('manager', null);
    }
}