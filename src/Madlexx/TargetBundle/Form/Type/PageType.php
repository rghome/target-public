<?php

namespace Madlexx\TargetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\TargetBundle\Entity\Page;

/**
 * Class PageType
 *
 * @package Madlexx\TargetBundle\Form\Type
 */
class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'madlexx_target.page.title'
            ]
        ])->add('content', TextareaType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'madlexx_target.page.content'
            ]
        ])->add('submit', SubmitType::class, [
            'label' => 'madlexx_target.actions.submit'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Page::class);
    }
}
