<?php

namespace Madlexx\TargetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\TargetBundle\Form\Transformer\StringToDateTimeTransformer;
use Madlexx\TargetBundle\Entity\Event;

/**
 * Class EventType
 *
 * @package Madlexx\TargetBundle\Form\Type
 */
class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'madlexx_target.event.form.title'
            ]
        ])->add('content', TextareaType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'madlexx_target.event.form.content'
            ]
        ])->add('date', TextType::class, [
            'label' => false,
            'attr'  => [
                'placeholder' => date('d.m.Y g:i a')
            ]
        ])->add('submit', SubmitType::class, [
            'label' => 'madlexx_target.event.actions.create'
        ]);

        $builder->get('date')->addModelTransformer(new StringToDateTimeTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Event::class);
    }
}
