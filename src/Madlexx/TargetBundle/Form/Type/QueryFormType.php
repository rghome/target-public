<?php

namespace Madlexx\TargetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\TargetBundle\Entity\Query;

/**
 * Class QueryFromType
 *
 * @package Madlexx\TargetBundle\Form\Type
 */
class QueryFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => false,
            'attr'  => [
                'placeholder' => 'madlexx_target.query.form.name'
            ]
        ])->add('content', TextareaType::class, [
            'label' => false,
            'attr'  => [
                'placeholder' => 'madlexx_target.query.form.content'
            ]
        ])->add('confidential', CheckboxType::class, [
            'label' => 'madlexx_target.query.form.query.confidential.label',
            'required' => false
        ])->add('forPublication', CheckboxType::class, [
            'label' => 'madlexx_target.query.form.query.forPublication.label',
            'required' => false
        ])->add('submit', SubmitType::class, [
            'label' => 'madlexx_target.actions.submit'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Query::class);
    }
}
