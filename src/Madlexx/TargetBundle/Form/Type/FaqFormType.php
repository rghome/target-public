<?php

namespace Madlexx\TargetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Madlexx\TargetBundle\Entity\Faq;

/**
 * Class FaqFormType
 *
 * @package Madlexx\TargetBundle\Form\Type
 */
class FaqFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('question', TextType::class, [
            'label' => false,
            'attr'  => [
                'placeholder' => 'madlexx_target.faq.form.question.label'
            ],
        ])->add('answer', TextareaType::class, [
            'label'     => 'madlexx_target.faq.form.answer.label',
            'required'  => false
        ])->add('position', NumberType::class, [
            'label' => false,
            'attr'  => [
                'placeholder' => 'madlexx_target.faq.form.position.label',
            ],
        ])->add('enabled', CheckboxType::class, [
            'label' => 'madlexx_target.faq.form.enabled.label',
            'required' => false
        ])->add('submit', SubmitType::class, [
            'label' => $options['entity_id'] ? 'madlexx_target.faq.form.update' : 'madlexx_target.faq.form.create'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Faq::class,
            'entity_id' => null
        ]);
    }
}
