<?php

namespace Madlexx\TargetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\Page;
use Madlexx\TargetBundle\Form\Type\PageType;

/**
 * Class PageController
 *
 * @package Madlexx\TargetBundle\Controller
 */
class PageController extends Controller
{
    /**
     * @Route("/page/edit", name = "madlexx_target.page.edit")
     * @Template()
     * @param Request $request
     *
     * @return array
     */
    public function editAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->get('doctrine.orm.default_entity_manager');
        $page = current($em->getRepository(Page::class)
            ->findAll());

        if (!$page) {
            $page = $this->createPage();
        }

        $form = $this->createForm(PageType::class, $page);
        $message = '';

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($page);
            $em->flush();

            $message = $this->get('translator')->trans(
                'madlexx_target.page.updated'
            );
        }

        $form = $form->createView();

        return compact('message', 'form');
    }

    /**
     * @return Page
     */
    private function createPage()
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $page = new Page();
        $page->setTitle('About us page');
        $page->setContent('Content');


        $em->persist($page);
        $em->flush();

        return $page;
    }
}
