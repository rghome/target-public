<?php

namespace Madlexx\TargetBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\Event;
use Madlexx\TargetBundle\Form\Type\EventType;

/**
 * Class EventController
 *
 * @package Madlexx\TargetBundle\Controller.
 */
class EventController extends Controller
{
    /**
     * @Route("/events/events", name = "madlexx_target.event.get_events")
     * @Template("MadlexxTargetBundle:Dashboard:events.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function getEventsAction(Request $request)
    {
        $events = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Event::class)
            ->getEventMonth(
                $request->request->get('currentMonth', 'm'),
                $request->request->get('currentDay', null),
                $this->getUser()
            );

        return compact('events');
    }

    /**
     * @Route("/events/days", name = "madlexx_target.event.get_days")
     * @param Request $request
     *
     * @return Response
     */
    public function getDaysAction(Request $request)
    {
        $events = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Event::class)
            ->getDays(
                $request->request->get('currentMonth', 1),
                $this->getUser()
            );

        $days = [];
        foreach ($events as $day) {
            $days[] = $day['day'];
        }

        $days = implode(',', $days);

        return new Response($days);
    }

    /**
     * @Route("/events/form", name = "madlexx_target.event.form", condition="request.isXmlHttpRequest()")
     * @Template()
     *
     * @return array
     */
    public function createFormAction()
    {
        $form = $this->createForm(EventType::class, new Event());

        $form = $form->createView();

        return compact('form');
    }

    /**
     * @Route("/events/create", name = "madlexx_target.event.create", condition="request.isXmlHttpRequest()")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createEvent(Request $request)
    {
        $translator = $this->get('translator');
        $event = new Event();
        $manager = $this->getUser();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        $status = 400;

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->get('doctrine.orm.default_entity_manager');

            if ($manager->getDirection()) {
                $event->setDirection($manager->getDirection());
            }

            $em->persist($event);
            try {
                $em->flush();
                $status = 201;
                $message = $translator->trans(
                    'madlexx_target.event.form.saved',
                    [
                        '%event%' => $form->getData()
                            ->getTitle(),
                    ]
                );
            } catch (UniqueConstraintViolationException $e) {
                $message = $e->getMessage();
            }
        } else {
            $message = $this->getErrorsString($form);
        }

        return new JsonResponse(
            [
                'message' => $message,
            ],
            $status
        );

    }

    /**
     * @param Form $form
     *
     * @return string
     */
    private function getErrorsString($form)
    {
        $errors = $this->get('validator')->validate($form);
        $output = '';
        foreach ($errors as $key => $error) {
            $output .= $error->getMessage();
        }

        return $output;
    }
}
