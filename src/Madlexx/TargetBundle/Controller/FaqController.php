<?php

namespace Madlexx\TargetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Madlexx\TargetBundle\Form\Type\FaqFormType;
use Madlexx\TargetBundle\Entity\Faq;
use Madlexx\TargetBundle\Entity\Page;

/**
 * Class FaqController
 *
 * @package Madlexx\TargetBundle\Controller
 */
class FaqController extends Controller
{
    /**
     * @Route("/faq/show", name="madlexx_target.faq.show")
     * @Template()
     * @return array|Faq[]
     */
    public function showAction()
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $about = current($em->getRepository(Page::class)->findAll());

        $faqs = $em
            ->getRepository(Faq::class)
            ->findBy(['enabled' => true], ['position' => 'ASC']);

        return compact('faqs', 'about');
    }
    /**
     * @Route("/faqs/all", name="madlexx_target.faq.index")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);

        $faqs = $this->get('serializer')
            ->serialize(
                $this->get('doctrine.orm.default_entity_manager')
                    ->getRepository(Faq::class)
                    ->findAll(),
                'json'
            );

        return compact('faqs');
    }

    /**
     * @Route("/faqs/create", name="madlexx_target.faq.create")
     * @Template()
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);

        $faq = new Faq();

        $form = $this->createForm(FaqFormType::class, $faq);

        $response = $this->get('madlexx_target.handler.faq')
            ->handle($form, $request, $faq);

        if ($response['success']) {
            $this->get('session')
                ->getFlashBag()
                ->add(
                    'success',
                    $this->get('translator')
                        ->trans('madlexx_target.faq.form.success.created', ['%question%' => $faq->getQuestion()])
                );

            return $this->redirectToRoute('madlexx_target.faq.index');
        }

        $response['message'] = $form->getErrors(true);

        return $response;
    }

    /**
     * @Route("/faqs/edit/{id}", name="madlexx_target.faq.edit")
     * @Template()
     * @param Faq     $faq
     * @param Request $request
     * @ParamConverter("faq", class="MadlexxTargetBundle:Faq")
     *
     * @return array|RedirectResponse
     */
    public function editAction(Faq $faq, Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);

        $form = $this->createForm(FaqFormType::class, $faq, [
            'entity_id' => $faq->getId()
        ]);

        $response = $this->get('madlexx_target.handler.faq')
            ->handle($form, $request, $faq);

        if ($response['success']) {
            $this->get('session')
                ->getFlashBag()
                ->add(
                    'success',
                    $this->get('translator')
                        ->trans('madlexx_target.faq.form.success.updated', ['%question%' => $faq->getQuestion()])
                );

            return $this->redirectToRoute('madlexx_target.faq.index');
        }

        $response['message'] = $form->getErrors(true);

        return $response;
    }

    /**
     * @Route("/faqs/delete/{id}", name="madlexx_target.faqs.delete", condition="request.isXmlHttpRequest()")
     * @ParamConverter("faq", class="MadlexxTargetBundle:Faq")
     * @param Faq $faq
     *
     * @return JsonResponse
     */
    public function deleteAction(Faq $faq)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);

        try {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($faq);
            $em->flush();

            $translator = $this->get('translator');

            $message = $translator->trans(
                'madlexx_target.faq.messages.removed',
                ['%question%' => $faq->getQuestion()]
            );
            $status = 200;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status = 500;
        }

        return new JsonResponse(
            [
                'message' => $message
            ],
            $status
        );
    }
}
