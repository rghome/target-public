<?php


namespace Madlexx\TargetBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\MostViews;
use Madlexx\TargetBundle\Entity\File;


/**
 * Class MostViewsController
 * @package Madlexx\TargetBundle\Controller
 */
class MostViewsController extends Controller
{
    /**
     * @Route(
     *     "/most-views/all",
     *     name="madlexx_target.most_views.index"
     * )
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $sorting = $request->query->get('sorting', 'DESC');
        $limit = 5;

        $content = $this->get('doctrine.orm.default_entity_manager')
              ->getRepository(MostViews::class)
              ->findBy(
                  [
                      'user' => $this->getUser()->getId(),
                  ],
                  ['views' => $sorting], $limit
              )
        ;

        return new JsonResponse(compact('content'));
    }

    /**
     * @Route("/most-views/create/{id}", name="madlexx_target.most_views.create", condition="request.isXmlHttpRequest()")
     * @ParamConverter("file", class="MadlexxTargetBundle:File")
     * @Template()
     * @param File $file
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(File $file, Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $view = $em->getRepository(MostViews::class)
            ->findOneBy(
                [
                    'user' =>  $this->getUser()->getId(),
                    'file' => $file->getId()
                ]
            )
        ;

        $mostViews = new MostViews();
        if (!$view instanceof $mostViews) {
            $view = $mostViews;
            $view->setFile($file);
            $view->setUser($this->getUser());
            $view->setViews(1);
        } else {
            $valueToIncrement = $view->getViews();
            $view->setViews(++$valueToIncrement);
        }

        $em->persist($view);
        $em->flush();

        return new JsonResponse([]);
    }
}