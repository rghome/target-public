<?php

namespace Madlexx\TargetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DashboardController
 *
 * @package Madlexx\TargetBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * @Route("/", name="madlexx_target.main")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
        return [];
    }
}
