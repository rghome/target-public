<?php

namespace Madlexx\TargetBundle\Controller;

use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\TargetBundle\Form\Type\ReportOrganizationsType;
use Madlexx\TargetBundle\Service\ReportService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\File;
use Madlexx\TargetBundle\Entity\Report;
use Madlexx\TargetBundle\Form\Type\FileFormType;
use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class ReportDocument
 *
 * @Route(service="madlexx_target.controller.report_controller")
 *
 * @package Madlexx\TargetBundle\Controller
 */
class ReportController extends Controller
{
    /**
     * @var ReportService
     */
    private $reportService;

    /**
     * ReportController constructor.
     *
     * @param ReportService $reportService
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * @Route(
     *     "/report/all",
     *     name="madlexx_target.report.index"
     * )
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $sorting = $request->query->get('sorting', 'DESC');
        $offset = $request->query->get('offset', 0);
        $limit = 100;

        /** @var User $user */
        $user = $this->getUser();

        return new JsonResponse([
            'reports' => $this->reportService->findMoreThenOrganizationCreated(
                $user->getOrganization(),
                $sorting, $limit, $offset
            )
        ]);
    }

    /**
     * @Route(
     *     "/report/quarter",
     *     name="madlexx_target.report.quarter"
     * )
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function quarterAction(Request $request)
    {
        $sorting = $request->query->get('sorting', 'DESC');
        $offset = $request->query->get('offset', 0);
        $limit = 100;

        /** @var User $user */
        $user = $this->getUser();

        $reports = $this->reportService->findMoreThenOrganizationCreated(
            $user->getOrganization(),
            $sorting, $limit, $offset
        );

        $reportsCache = [];
        $template = 'MadlexxTargetBundle:Dashboard/Form:report_organizations_form.html.twig';
        foreach ($reports as $i => $report) {
            $form = $this->createForm(ReportOrganizationsType::class, $report, [
                'manager' => $this->getUser()
            ]);

            $reportsCache[$i]['report'] = $report;
            $reportsCache[$i]['form'] = $this->renderView($template, [
                'form' => $form->createView()
            ]);
        }

        $organizationsCount = $this->getOrganizationsCount($user);
        return new JsonResponse(
            [
                'reports' => $reportsCache,
                'organizationsCount' => $organizationsCount
            ]
        );
    }

    /**
     * @Route("/report/create", name="madlexx_target.report.create", condition="request.isXmlHttpRequest()")
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $file = new File();
        $manager = $this->getUser();
        $form = $this->createForm(FileFormType::class, $file);

        $response = $this->get('madlexx_target.handler.file')
            ->handleReport($form, $request, $file, $manager);

        if ($response['success']) {
            return new JsonResponse('success');
        }

        return new JsonResponse([
            'form' => $this->renderView('MadlexxTargetBundle:Dashboard/Form:_file_form.html.twig', [
                'form' => $form->createView(),
                'idCss' => 'report_file_form',
                'label' => $this->get('translator')->trans('madlexx_target.reports.actions.drop'),
                'url'   => $this->generateUrl('madlexx_target.report.create')
            ]),
        ]);
    }

    /**
     * @Route("/report/set-organizations/{id}", name="madlexx_target.report.set_organizations", condition="request.isXmlHttpRequest()")
     * @ParamConverter("report", class="MadlexxTargetBundle:Report")
     * @Template()
     * @param Report $report
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setOrganizationsAction(Report $report, Request $request)
    {
        $form = $this->createForm(ReportOrganizationsType::class, $report, [
            'manager' => $this->getUser()
        ]);

        $form->handleRequest($request);

        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->persist($report);
        $em->flush();

        $organizationsCount = $this->getOrganizationsCount($this->getUser());

        $message = $this->get('translator')
            ->trans(
                'madlexx_target.reports.organizationsUpdate'
            )
        ;

        return new JsonResponse(compact('report', 'organizationsCount', 'message'));

    }

    /**
     * @Route("/report/remove/{id}", name="madlexx_target.report.remove", condition="request.isXmlHttpRequest()")
     * @param Report $report
     * @ParamConverter("report", class="MadlexxTargetBundle:Report")
     * @return JsonResponse
     */
    public function removeAction(Report $report)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        try {
            $this->get('madlexx_target.handler.file')->removeFile($report->getFile());
            $em->remove($report);
            $em->flush();
            $message = $this->get('translator')
                ->trans(
                    'madlexx_target.reports.actions.removed',
                    [
                        '%report%' => $report->getName(),
                    ]
                );
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(compact('message'));
    }

    /**
     * @param User $user
     *
     * @return int
     */
    private function getOrganizationsCount(User $user)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $organizationCount = 0;
        if ($direction = $user->getDirection()) {
            $organizationCount = count($em->getRepository(Organization::class)
                  ->findBy(
                      [
                          'direction' => $direction->getId()
                      ]
                  ))
            ;
        }

        return $organizationCount;
    }
}
