<?php

namespace Madlexx\TargetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\Report;
use Madlexx\TargetBundle\Entity\Document;
use Madlexx\TargetBundle\Entity\Response;

/**
 * Class SearchController
 *
 * @package Madlexx\TargetBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * @Route("/search", name="madlexx_target.search.search")
     * @Template()
     * @param Request $request
     *
     * @return array
     */
    public function searchAction(Request $request)
    {
        $needle = $request->request->get('search', null);

        $em = $this->get('doctrine.orm.default_entity_manager');
        $reports = '';
        $documents = '';
        $responses = '';

        if (!is_null($needle)) {
            $reports = $em->getRepository(Report::class)->search($needle);
            $documents = $em->getRepository(Document::class)->search($needle);
        }

        return compact('reports', 'needle', 'documents', 'responses');
    }
}
