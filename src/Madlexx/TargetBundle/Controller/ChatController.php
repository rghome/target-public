<?php

namespace Madlexx\TargetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Madlexx\TargetBundle\Entity\Chat;
use Madlexx\TargetBundle\Entity\Message;

/**
 * Class ChatController
 *
 * @package Madlexx\TargetBundle\Controller
 */
class ChatController extends Controller
{
    /**
     * @Route("/chat/get", name="madlexx_target.chat.get")
     * @return JsonResponse
     */
    public function getMessages()
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $userId = $this->getUser()->getId();
        $chat = current($em->getRepository(Chat::class)->getByUser($userId));
        if (!$chat) {
            $chat = $this->createChat();
        }
        $messages = $chat->getMessages();
        $count = $em->getRepository(Chat::class)->countUnread($chat->getId(), $userId);

        return new JsonResponse([
            'messages' => $messages->toArray(),
            'count'    => (int) $count,
            'user'     => (int) $userId,
            'chat'     => (int) $chat->getId()
        ]);
    }

    /**
     * @Route("/chat/get/{id}", name="madlexx_target.chat.get_by_id")
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getMessagesByChat($id = 0)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $chat = current($em->getRepository(Chat::class)->getByChat($id));
        $messages = $chat->getMessages();

        return new JsonResponse([
            'messages' => $messages->toArray(),
        ]);
    }
    /**
     * @Route("/chat/get-all", name="madlexx_target.chat.get_all")
     * @return JsonResponse
     */
    public function getAllChats()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);

        $filters = [];

        if ($this->getUser()->getDirection()) {
            $filters['direction'] = $this->getUser()->getDirection()->getId();
        }

        $chats = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Chat::class)
            ->findBy($filters);

        $user = $this->getUser()->getId();

        return new JsonResponse(compact('chats', 'user'));
    }

    /**
     * @Route("/chat/message/create", name="madlexx_target.chat_message.create")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createMessage(Request $request)
    {
        $message = $request->request->get('message');
        $chatId = $request->request->get('chat');

        $em = $this->get('doctrine.orm.default_entity_manager');

        $chat = $em->getRepository(Chat::class)->find($chatId);

        $new = new Message();
        $new->setContent($message);
        $new->setUser($this->getUser());
        $new->setChat($chat);
        $new->setSeen(false);

        if ($this->getUser()->getDirection()) {
            $new->setDirection($this->getUser()->getDirection());
        }

        $em->persist($new);
        $em->flush();

        return new JsonResponse([
            'message' => $new
        ]);
    }

    /**
     * @Route("/chat/message/clear-count", name="madlexx_target.chat_message.clear_count")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function clearCount(Request $request)
    {
        $chat = $request->request->get('chat');
        $user = $this->getUser()->getId();

        $em = $this->get('doctrine.orm.default_entity_manager');

        $chat = $em->getRepository(Chat::class)->getUnread($chat, $user);

        $messages = $chat->getMessages();

        foreach ($messages as $message) {
            $message->setSeen(true);
            $em->persist($message);
        }
        $em->flush();

        return new JsonResponse([
            'count' => 0
        ]);
    }

    /**
     * @Route("/chat/upload", name="madlexx_target.chat_message.upload")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadFile(Request $request)
    {
        if (!$this->get('madlexx_target.handler.file')
            ->handleChatUpload($request, $this->getUser())
        ) {
            return new JsonResponse([
                'message' => 'Error when upload happen.'
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse([
            'message' => 'Successful uploaded.'
        ]);
    }
    /**
     * @return Chat
     */
    private function createChat()
    {
        $chat = new Chat();
        $chat->setUser($this->getUser());
        $em = $this->get('doctrine.orm.default_entity_manager');

        if ($this->getUser()->getDirection()) {
            $chat->setDirection($this->getUser()->getDirection());
        }

        $em->persist($chat);
        $em->flush();

        return $chat;
    }
}
