<?php

namespace Madlexx\TargetBundle\Controller;

use Madlexx\TargetBundle\Form\Type\QueryFormType;
use Madlexx\TargetBundle\Service\QueryService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\Query;
use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class QueryController
 *
 * @Route(service="madlexx_target.controller.query_controller")
 *
 * @package Madlexx\TargetBundle\Controller
 */
class QueryController extends Controller
{
    /**
     * @var QueryService
     */
    private $queryService;

    /**
     * QueryController constructor.
     *
     * @param QueryService $queryService
     */
    public function __construct(QueryService $queryService)
    {
        $this->queryService = $queryService;
    }

    /**
     * @Route("/query/all-confidential", name="madlexx_target.query.index")
     * @return JsonResponse
     */
    public function indexAction()
    {
        $filters = ['confidential' => false];

        if ($this->getUser()->getDirection()) {
            $filters['direction'] = $this->getUser()->getDirection()->getId();
        }

        $queries = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Query::class)
            ->findBy($filters, ['id' => 'DESC']);

        return new JsonResponse(compact('queries'));
    }

    /**
     * @Route("/query/personal", name="madlexx_target.query.index_personal")
     * @return JsonResponse
     */
    public function indexPersonalAction()
    {
        $user = $this->getUser()->getId();

        $filters = ['user' => $user];

        if ($this->getUser()->getDirection()) {
            $filters['direction'] = $this->getUser()->getDirection()->getId();
        }

        $queries = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Query::class)
            ->findBy($filters);


        return new JsonResponse(compact('queries', 'user'));
    }

    /**
     * @Route("/query/organization", name="madlexx_target.query.index_organization")
     * @return JsonResponse
     */
    public function indexOrganizationAction()
    {
        $queries = [];
        $user = $this->getUser();
        if (is_null(
            $user
                ->getOrganization()
        )) {
            $user = $user->getId();
            return new JsonResponse(compact('queries', 'user'));
        }

        $filters = [
            'confidential' => true,
            'organization' => $this->getUser()->getOrganization()->getId(),
        ];

        if ($this->getUser()->getDirection()) {
            $filters['direction'] = $this->getUser()->getDirection()->getId();
        }

        $queries = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Query::class)
            ->findBy($filters);

        $user = $user->getId();
        return new JsonResponse(compact('queries', 'user'));
    }

    /**
     * @Route("/query/shared", name="madlexx_target.query.index_shared")
     * @return JsonResponse
     */
    public function indexSharedAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $queries = $this->queryService
            ->findMoreThenOrganizationCreated($user->getOrganization(), 'DESC');

        $user = $this->getUser()->getId();

        return new JsonResponse(compact('queries', 'user'));
    }

    /**
     * @Route("/query/all", name="madlexx_target.query.index_all")
     * @return JsonResponse
     */
    public function listAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $filters = [];

        if ($this->getUser()->getDirection()) {
            $filters['direction'] = $this->getUser()->getDirection()->getId();
        }

        $queries = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Query::class)
            ->findBy($filters, ['id'=>'DESC']);

        return new JsonResponse([
            'queries'   => $queries,
            'user'      => $this->getUser()->getId()
        ]);
    }

    /**
     * @Route("/query/not-responded", name="madlexx_target.query.not_responded")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listNotRespondedAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $offset = $request->query->getInt('offset', 0);

        $manager = $this->getUser();

        return new JsonResponse([
            'queries'   => $this->get('doctrine.orm.default_entity_manager')
                ->getRepository(Query::class)
                ->getLastNotResponded($offset, 5, $manager),
            'user' => $manager->getId()
        ]);
    }

    /**
     * @Route("/query/create", name="madlexx_target.query.create")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $query = new Query();
        $form = $this->createForm(QueryFormType::class, $query);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $user = $this->getUser();
            $query->setUser($user);
            $query->setOrganization($user->getOrganization());
            $query->setViewed(false);

            if ($user->getDirection()) {
                $query->setDirection($user->getDirection());
            }

            $em->persist($query);
            $em->flush();

            $form = $this->createForm(QueryFormType::class, new Query());
        }

        return new JsonResponse([
            'form' => $this->renderView('MadlexxTargetBundle:Dashboard/Form:_query_form.html.twig', [
                'form' => $form->createView()
            ]),
            'query' => $query->getId() ? $query : null,
            'message' => $this->get('translator')
                ->trans('madlexx_target.query.success', ['%name%' => $query->getName()])
        ]);
    }
}
