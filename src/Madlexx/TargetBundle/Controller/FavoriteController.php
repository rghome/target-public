<?php


namespace Madlexx\TargetBundle\Controller;

use Madlexx\TargetBundle\Entity\Favorite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\File;

/**
 * Class FavoriteController
 * @package Madlexx\TargetBundle\Controller
 */
class FavoriteController extends Controller
{
    /**
     * @Route(
     *     "/favorite/all",
     *     name="madlexx_target.favorite.index"
     * )
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $sorting = $request->query->get('sorting', 'DESC');
        $offset = $request->query->get('offset', 0);
        $limit = 5;

        $favorites = $this->get('doctrine.orm.default_entity_manager')
             ->getRepository(Favorite::class)
             ->findBy(
                 [
                     'user' => $this->getUser()->getId(),
                 ],
                 ['id' => 'DESC']
             )
        ;

        return new JsonResponse(compact('favorites'));
    }

    /**
     * @Route("/favorite/is/{id}", name="madlexx_target.favorite.is", condition="request.isXmlHttpRequest()")
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function isAction($id, Request $request)
    {
        return new JsonResponse([
            'is' => $this->get('doctrine.orm.default_entity_manager')
                ->getRepository(Favorite::class)
                ->findBy(
                    [
                        'file' => $id,
                        'user' => $this->getUser()->getId(),
                    ],
                    ['id' => 'DESC']
                )
        ]);
    }

    /**
     * @Route("/favorite/create/{id}", name="madlexx_target.favorite.create", condition="request.isXmlHttpRequest()")
     * @ParamConverter("file", class="MadlexxTargetBundle:File")
     * @Template()
     * @param File $file
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(File $file, Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        try {
            $favorite = new Favorite();
            $favorite->setFile($file);
            $favorite->setUser($this->getUser());

            $em->persist($favorite);
            $em->flush();

            $message = $this->get('translator')->trans(
                'madlexx_target.favorites.actions.create',
                [
                    '%filename%' => $file->getName(),
                ]
            );
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(compact('message'));
    }

    /**
     * @Route("/favorite/remove/{id}", name="madlexx_target.favorite.remove", condition="request.isXmlHttpRequest()")
     * @param File $file
     * @ParamConverter("file", class="MadlexxTargetBundle:File")
     * @return JsonResponse
     */
    public function removeAction(File $file)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $repository = $em->getRepository(Favorite::class);

        try {
            /** @var Favorite $favorite */
            $favorite = $repository->findOneBy(
                [
                    'file' => $file->getId(),
                    'user' => $this->getUser()->getId()
                ]
            );

            $message = $this->get('translator')->trans(
                'madlexx_target.favorites.actions.removed',
                [
                    '%filename%' => $file->getName(),
                ]
            );

            $em->remove($favorite);
            $em->flush();
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(compact('message'));
    }
}