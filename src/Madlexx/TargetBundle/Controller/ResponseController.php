<?php

namespace Madlexx\TargetBundle\Controller;

use Madlexx\FOSUserBundle\Entity\User;
use Madlexx\TargetBundle\Service\QueryService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Madlexx\TargetBundle\Form\Type\FileFormType;
use Madlexx\TargetBundle\Entity\File;
use Madlexx\TargetBundle\Entity\Query;
use Madlexx\TargetBundle\Entity\Response;

/**
 * Class ResponseController
 *
 * @Route(service="madlexx_target.controller.response_controller")
 *
 * @package Madlexx\TargetBundle\Controller
 */
class ResponseController extends Controller
{
    /**
     * @var QueryService
     */
    private $queryService;

    /**
     * ResponseController constructor.
     *
     * @param QueryService $queryService
     */
    public function __construct(QueryService $queryService)
    {
        $this->queryService = $queryService;
    }

    /**
     * @Route("/response/create/{id}", name="madlexx_target.response.create", condition="request.isXmlHttpRequest()")
     * @ParamConverter("query", class="MadlexxTargetBundle:Query")
     * @param Query   $query
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Query $query, Request $request)
    {
        $file = new File();
        $form = $this->createForm(FileFormType::class, $file);

        $response = $this->get('madlexx_target.handler.file')
            ->handleResponse($form, $request, $file, $query, $this->getUser());

        if ($this->isGranted('ROLE_SUPER_ADMIN') || $this->isGranted('ROLE_ADMIN')) {
            $this->queryService->setViewed($query, true);
        }

        if ($response['success']) {
            $response = $response['response'];
            return new JsonResponse(compact('response'));
        }

        return new JsonResponse([
            'form'      => $this->renderView('MadlexxTargetBundle:Dashboard/Form:_file_form.html.twig', [
                'form'  => $form->createView(),
                'idCss' => 'response_file_form',
                'label' => $this->get('translator')->trans('madlexx_target.response.actions.drop'),
                'url'   => $this->generateUrl('madlexx_target.response.create', ['id' => $query->getId()])
            ]),
            'response'  => $query->getResponse()->toArray(),
            'query'     => $query,
        ]);
    }

    /**
     * @Route("/response/upload", name="madlexx_target.response.upload")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadFile(Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $query = $em->getRepository(Query::class)
            ->findOneBy(
                ['id' => $request->request->get('query')]
            )
        ;

        if (!$response = $this->get('madlexx_target.handler.file')
                  ->handleResponseUpload($request, $this->getUser(), $query)
        ) {
            return new JsonResponse([
                'message' => 'Error when upload happen.'
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($this->isGranted('ROLE_SUPER_ADMIN') || $this->isGranted('ROLE_ADMIN')) {
            $this->queryService->setViewed($query, true);
        }

        return new JsonResponse([
            'message' => 'Successful uploaded.',
            'response' => $response
        ]);
    }

    /**
     * @Route("/response/create-message", name="madlexx_target.response.create_message", condition="request.isXmlHttpRequest()")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createMessage(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', User::ROLE_SECONDARY_USER]);

        $content = $request->get('message');
        $queryId = $request->get('id');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $query = $em->getRepository(Query::class)->findOneBy(['id' => $queryId]);
        $response = new Response();

        $response->setQuery($query);
        $response->setName('message');
        $response->setContent($content);
        $response->setMessageType(true);
        $response->setUser($this->getUser());

        if ($this->getUser()->getDirection()) {
            $response->setDirection($this->getUser()->getDirection());
        }

        $em->persist($response);
        $em->flush();

        return new JsonResponse(compact('response'));
    }
}
