<?php

namespace Madlexx\TargetBundle\Controller;

use Madlexx\TargetBundle\Service\DocumentService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Madlexx\TargetBundle\Entity\Document;
use Madlexx\TargetBundle\Entity\File;
use Madlexx\TargetBundle\Form\Type\FileFormType;
use Madlexx\FOSUserBundle\Entity\User;

/**
 * Class DocumentController
 *
 * @Route(service="madlexx_target.controller.document_controller")
 *
 * @package Madlexx\TargetBundle\Controller
 */
class DocumentController extends Controller
{
    /**
     * @var DocumentService
     */
    private $documentService;

    /**
     * DocumentController constructor.
     *
     * @param DocumentService $documentService
     */
    public function __construct(DocumentService $documentService)
    {
        $this->documentService = $documentService;
    }

    /**
     * @Route("/document/all", name="madlexx_target.document.index")
     * @Template()
     * @return JsonResponse
     */
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        return new JsonResponse([
            'documents' => $this->documentService
                ->findMoreThenOrganizationCreated($user->getOrganization(), 'DESC')
        ]);
    }

    /**
     * @Route("/document/create", name="madlexx_target.document.create", condition="request.isXmlHttpRequest()")
     * @Template()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $file = new File();
        $manager = $this->getUser();
        $form = $this->createForm(FileFormType::class, $file);

        $response = $this->get('madlexx_target.handler.file')
            ->handleDocument($form, $request, $file, $manager);

        if ($response['success']) {
            return new JsonResponse('success');
        }

        return new JsonResponse([
            'form' => $this->renderView('MadlexxTargetBundle:Dashboard/Form:_file_form.html.twig', [
                'form' => $form->createView(),
                'idCss' => 'document_file_form',
                'label' => $this->get('translator')->trans('madlexx_target.documents.actions.drop'),
                'url'   => $this->generateUrl('madlexx_target.document.create')
            ]),
        ]);
    }

    /**
     * @Route("/document/remove/{id}", name="madlexx_target.document.remove", condition="request.isXmlHttpRequest()")
     * @param Document $document
     * @ParamConverter("report", class="MadlexxTargetBundle:Document")
     * @return JsonResponse
     */
    public function removeAction(Document $document)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        try {
            $this->get('madlexx_target.handler.file')->removeFile($document->getFile());
            $em->remove($document);
            $em->flush();
            $message = $this->get('translator')
                ->trans(
                    'madlexx_target.documents.actions.removed',
                    [
                        '%document%' => $document->getName(),
                    ]
                );
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse(compact('message'));
    }
}
