<?php


namespace Madlexx\TargetBundle\Service;


use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\TargetBundle\Entity\Repository\DocumentRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class DocumentService
 * @package Madlexx\TargetBundle\Service
 */
class DocumentService
{
    /**
     * @var DocumentRepository
     */
    private $documentRepository;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * ReportService constructor.
     *
     * @param AuthorizationChecker $authorizationChecker
     * @param DocumentRepository $documentRepository
     */
    public function __construct(
        AuthorizationChecker $authorizationChecker,
        DocumentRepository $documentRepository
    )
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param Organization|null $organization
     * @param null $orderBy
     *
     * @return array
     */
    public function findMoreThenOrganizationCreated(Organization $organization = null, $orderBy = null)
    {
        if (
            $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN') ||
            $this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->authorizationChecker->isGranted('ROLE_USER') && is_null($organization))
        ) {
            if (!is_null($organization)) {
                if ($organization->getDirection()) {
                    return $this->documentRepository->findBy(
                        [
                            'direction' => $organization->getDirection()->getId()
                        ],
                        ['id' => $orderBy]
                    );
                }
            }


            return $this->documentRepository->findBy([], ['id' => $orderBy]);
        }

        return $this->documentRepository->findMoreThenOrganizationCreated($organization, $orderBy);
    }
}