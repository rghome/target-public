<?php


namespace Madlexx\TargetBundle\Service;


use Doctrine\ORM\EntityManager;
use Madlexx\TargetBundle\Entity\Query;
use Madlexx\TargetBundle\Entity\Repository\QueryRepository;
use Madlexx\FOSUserBundle\Entity\Organization;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class QueryService
 * @package Madlexx\TargetBundle\Service
 */
class QueryService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var QueryRepository
     */
    private $queryRepository;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * QueryService constructor.
     *
     * @param EntityManager $entityManager
     * @param AuthorizationChecker $authorizationChecker
     * @param QueryRepository $queryRepository
     */
    public function __construct(
        EntityManager $entityManager,
        AuthorizationChecker $authorizationChecker,
        QueryRepository $queryRepository
    )
    {
        $this->em = $entityManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->queryRepository = $queryRepository;
    }

    /**
     * @param Query $query
     * @param bool $viewed
     */
    public function setViewed(Query $query, $viewed = true)
    {
        if ($query->isViewed() !== $viewed) {
            $query->setViewed($viewed);

            $this->em->persist($query);
            $this->em->flush();
        }
    }

    /**
     * @param Organization|null $organization
     * @param null $orderBy
     *
     * @return array
     */
    public function findMoreThenOrganizationCreated(Organization $organization = null, $orderBy = null)
    {
        if (
            $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN') ||
            $this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->authorizationChecker->isGranted('ROLE_USER') && is_null($organization))
        ) {
            if ($organization->getDirection()) {
                return $this->queryRepository->findBy(
                    [
                        'direction' => $organization->getDirection()->getId()
                    ],
                    ['id' => $orderBy]
                );
            }

            return $this->queryRepository->findBy(['confidential' => false], ['id' => $orderBy]);
        }

        return $this->queryRepository->findMoreThenOrganizationCreated($organization, $orderBy);
    }
}