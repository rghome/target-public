<?php


namespace Madlexx\TargetBundle\Service;

use Madlexx\FOSUserBundle\Entity\Organization;
use Madlexx\TargetBundle\Entity\Repository\ReportRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


/**
 * Class ReportService
 * @package Madlexx\TargetBundle\Service
 */
class ReportService
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * ReportService constructor.
     *
     * @param AuthorizationChecker $authorizationChecker
     * @param ReportRepository $reportRepository
     */
    public function __construct(
        AuthorizationChecker $authorizationChecker,
        ReportRepository $reportRepository
    )
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->reportRepository = $reportRepository;
    }

    /**
     * @param Organization|null $organization
     * @param null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @return array
     */
    public function findMoreThenOrganizationCreated(
        Organization $organization = null,
        $orderBy = null, $limit = null, $offset = null
    )
    {
        if (
            $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN') ||
            $this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->authorizationChecker->isGranted('ROLE_USER') && is_null($organization))
        ) {
            if (!is_null($organization)) {
                if ($organization->getDirection()) {
                    return $this->reportRepository->findBy(
                        [
                            'direction' => $organization->getDirection()->getId()
                        ],
                        ['id' => $orderBy],
                        $limit,
                        $offset
                    );
                }
            }

            return $this->reportRepository->findBy([], ['id' => $orderBy], $limit, $offset);
        }

        return $this->reportRepository->findMoreThenOrganizationCreated($organization, $orderBy, $limit, $offset);
    }
}